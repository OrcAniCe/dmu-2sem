package arraylist;

import java.util.Iterator;

public class Main {

	public static void main(String[] args) {
		ArrayedList<String> list = new ArrayedList<>();

		list.add("Banan");
		list.add("Æble");
		list.add("Tomat");
		list.add("Jordbær");
		Iterator<String> iter = list.iterator();
		list.add("test");
		System.out.println(list);
		System.out.println();
		while (iter.hasNext()) {
			System.out.println(iter.next());
		}

	}

}
