package composite;

public abstract class Figure {

	public void add(Figure f) {
		throw new UnsupportedOperationException();
	}

	public void remove(Figure f) {
		throw new UnsupportedOperationException();
	}

	public String getName() {
		throw new UnsupportedOperationException();
	}

	public void setName() {
		throw new UnsupportedOperationException();
	}

	public void draw() {
		throw new UnsupportedOperationException();
	}

	public double getArea() {
		throw new UnsupportedOperationException();
	}

}
