package composite;

public class Trekant extends Figure {

	private String name;
	private double area;

	public Trekant(String name, int width, int height) {
		this.area = (width * height) / 2;
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getArea() {
		return area;
	}

}
