package composite;

import java.util.ArrayList;

public class CompositeFigure extends Figure {

	private ArrayList<Figure> components = new ArrayList<>();
	private String name;

	public CompositeFigure(String name) {
		this.name = name;
	}

	@Override
	public void draw() {
		for (Figure f : components) {
			System.out.println(f.getName());
		}
	}

	@Override
	public double getArea() {
		double result = 0;
		for (Figure f : components) {
			result += f.getArea();
		}
		return result;
	}

	@Override
	public void add(Figure figure) {
		components.add(figure);
	}

	@Override
	public void remove(Figure figure) {
		components.remove(figure);
	}
}
