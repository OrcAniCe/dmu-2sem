package composite;

public class ConcreteFigure extends Figure {

	private String name;
	private double area;

	public ConcreteFigure(String name, double area) {
		this.area = area;
		this.name = name;
	}

	@Override
	public void draw() {
		System.out.println(name);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public double getArea() {
		return area;

	}

}
