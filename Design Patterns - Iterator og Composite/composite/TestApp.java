package composite;

public class TestApp {

	public static void main(String[] args) {

		Figure star = new CompositeFigure("Star");

		star.add(new ConcreteFigure("Square", 20));
		star.add(new ConcreteFigure("Triangle", 20));
		star.add(new ConcreteFigure("Triangle", 20));
		star.add(new ConcreteFigure("Triangle", 20));
		star.add(new ConcreteFigure("Triangle", 20));

		star.draw();
		System.out.println(star.getArea());

	}
}
