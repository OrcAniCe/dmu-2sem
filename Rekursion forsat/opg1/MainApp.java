package opg1;

public class MainApp {

	public static boolean palindrom(String tekst) {
		return isPalindrom(tekst, 0, tekst.length() - 1);
	}

	public static boolean isPalindrom(String s, int start, int slut) {
		s.toLowerCase();
		// Forklaring?
		return slut - start <= 1 || s.charAt(start) == s.charAt(slut) && isPalindrom(s, start + 1, slut - 1);
	}

	public static void main(String[] args) {
		System.out.println(palindrom("naan"));
	}

}
