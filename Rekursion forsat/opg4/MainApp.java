package opg4;

public class MainApp {

	public static int binomial(int m, int n) {
		if (0 <= m && m <= n) {
			if (m == 0 || m == n) {
				return 1;
			}
		}
		return binomial(m, (n - 1)) + binomial((m - 1), (n - 1));
	}

	public static void main(String[] args) {
		System.out.println(binomial(3, 5));

	}

}
