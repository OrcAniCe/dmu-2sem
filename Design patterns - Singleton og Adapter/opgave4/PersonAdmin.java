package opgave4;

import java.util.HashSet;
import java.util.Set;

public class PersonAdmin {

	Set<Person> personSet = new HashSet<>();
	private static PersonAdmin persAdm;

	private PersonAdmin() {

	}

	public static PersonAdmin getInstance() {
		if (persAdm == null) {
			persAdm = new PersonAdmin();
		}
		return persAdm;
	}

	public void add(Person person) {
		personSet.add(person);
	}

	public void remove(Person person) {
		personSet.remove(person);
	}

	public Set<Person> getPerson() {
		return new HashSet<>(personSet);
	}

	public void initPersons() {
		personSet.add(new Person("Jack", "PVT"));
		personSet.add(new Person("Jones", "PVT"));
		personSet.add(new Person("Nichlas", "PFC"));
		personSet.add(new Person("Mitch", "Corporal"));
		personSet.add(new Person("Adrian", "PFC"));
		personSet.add(new Person("Mitchel", "Sargent"));

	}

	@Override
	public String toString() {
		return personSet + " ";
	}

}
