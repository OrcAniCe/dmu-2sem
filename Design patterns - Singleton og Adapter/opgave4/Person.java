package opgave4;

public class Person {

	private String name;
	private String rank;
	private String serialNo;
	private RandGenerator rand = new RandGenerator();

	public Person(String name, String rank) {
		this.name = name;
		this.rank = rank;
		this.serialNo = "MA" + rand.getSN();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	@Override
	public String toString() {
		return name + " " + rank + " " + serialNo;
	}

}
