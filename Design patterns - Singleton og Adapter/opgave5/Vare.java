package opgave5;

public class Vare implements MomsInterface {

	private int price;
	private String name;

	public Vare(String name, int price) {
		this.name = name;
		this.price = price;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public double beregnMoms() {
		// TODO Auto-generated method stub
		return 0;
	}

}
