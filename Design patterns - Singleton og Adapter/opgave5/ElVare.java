package opgave5;

public class ElVare extends Vare {

	public ElVare(String name, int price) {
		super(name, price);
	}

	@Override
	public double beregnMoms() {
		double moms = getPrice() * 0.30;
		if (moms < 3) {
			return 3;
		} else {
			return getPrice() * 0.30;
		}
	}

}
