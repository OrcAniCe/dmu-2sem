package opgave5;

public class Spiritus {

	private int price;
	private String description;

	public Spiritus(int price, String description) {
		this.description = description;
		this.price = price;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getDesc() {
		return description;
	}

	public void setDesc(String desc) {
		this.description = desc;
	}

	public double hentMoms() {
		return price * 0.15;
	}

}
