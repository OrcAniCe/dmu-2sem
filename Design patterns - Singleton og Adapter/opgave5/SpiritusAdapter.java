package opgave5;

public class SpiritusAdapter implements MomsInterface {
	Spiritus spiritus;

	public SpiritusAdapter(Spiritus spiritus) {
		this.spiritus = spiritus;

	}

	@Override
	public double beregnMoms() {
		return spiritus.hentMoms();

	}

}
