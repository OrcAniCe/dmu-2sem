package opgave5;

public class FoedeVare extends Vare {

	public FoedeVare(String name, int price) {
		super(name, price);
	}

	@Override
	public double beregnMoms() {
		return getPrice() * 0.05;
	}
}
