package opgave2;

public class CounterApp {
	public static void main(String[] args) {

		SingletonCounter sc = SingletonCounter.getInstance();
		SingletonCounter sc1 = SingletonCounter.getInstance();

		System.out.println(sc.getVal());

		for (int i = 0; i < 5; i++) {
			sc.count();
		}

		System.out.println(sc.getVal());

		sc.timesTwo();

		System.out.println(sc.getVal());

		sc.zero();

		System.out.println(sc.getVal());

		sc1.count();

		System.out.println(sc1.getVal());
		System.out.println(sc.getVal());

		System.out.println(sc.equals(sc1));

	}

}
