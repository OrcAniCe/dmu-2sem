package opgave2;

public class SingletonCounter {
	private static SingletonCounter uniqueInstance;
	private static int value = 0;

	public SingletonCounter() {
	}

	public static SingletonCounter getInstance() {
		if (uniqueInstance == null) {
			uniqueInstance = new SingletonCounter();
		}
		return uniqueInstance;
	}

	public void count() {
		value++;
	}

	public void timesTwo() {
		value = value * 2;
	}

	public void zero() {
		value = 0;
	}

	public int getVal() {
		return value;
	}

}
