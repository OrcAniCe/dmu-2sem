package service;

import java.util.ArrayList;

import model.Company;
import model.Employee;
import storage.Storage;

public class Service {

	private static Service service;
	Storage storage = Storage.getInstance();

	private Service() {
	}

	public static Service getInstance() {
		if (service == null) {
			service = new Service();
		}
		return service;
	}

	/**
	 * Creates a new Company.<br />
	 * Requires: hours >= 0.
	 */

	public Company createCompany(String name, int hours) {
		Company company = new Company(name, hours);
		storage.addCompany(company);
		return company;
	}

	/**
	 * Deletes the company.<br />
	 * Requires: The company has no employees.
	 */
	public void deleteCompany(Company company) {
		storage.removeCompany(company);
	}

	/**
	 * Updates the company.<br />
	 * Requires: hours >= 0.
	 */
	public void updateCompany(Company company, String name, int hours) {
		company.setName(name);
		company.setHours(hours);
	}

	/**
	 * Get all the companies
	 */
	public ArrayList<Company> getCompanies() {
		return storage.getCompanies();
	}

	// -------------------------------------------------------------------------

	/**
	 * Creates a new employee.<br />
	 * Requires: wage >= 0.
	 */
	public Employee createEmployee(String name, int wage) {
		Employee employee = new Employee(name, wage);
		storage.addEmployee(employee);
		return employee;
	}

	/**
	 * Deletes the employee.
	 */
	public void deleteEmployee(Employee employee) {
		Company company = employee.getCompany();
		if (company != null) {
			company.removeEmployee(employee);
			employee.clearCompany();
		}
		storage.removeEmployee(employee);
	}

	/**
	 * Updates the employee.<br />
	 * Requires: wage >= 0.
	 */
	public void updateEmployee(Employee employee, String name, int wage) {
		employee.setName(name);
		employee.setWage(wage);
	}

	/**
	 * Get all the employees.
	 */
	public ArrayList<Employee> getEmployees() {
		return storage.getEmployees();
	}

	// -------------------------------------------------------------------------

	/**
	 * Updates the employee's company.
	 */
	public void updateCompanyOfEmployee(Employee employee, Company company) {
		Company current = employee.getCompany();
		if (current != null) {
			current.removeEmployee(employee);
			employee.clearCompany();
		}
		employee.setCompany(company);
		company.addEmployee(employee);
	}

	/**
	 * Sets the company of the employee.
	 */
	public void setCompanyOfEmployee(Employee employee, Company company) {
		employee.setCompany(company);
		company.addEmployee(employee);
	}

	/**
	 * Clears the company of the employee.
	 */
	public void clearCompanyOfEmployee(Employee employee) {
		Company company = employee.getCompany();
		if (company != null) {
			company.removeEmployee(employee);
			employee.clearCompany();
		}
	}

	// -------------------------------------------------------------------------

	/**
	 * Initializes the storage with some objects.
	 */
	public void initStorage() {
		Company c1 = service.createCompany("IBM", 37);
		Company c2 = service.createCompany("AMD", 40);
		service.createCompany("SLED", 45);
		service.createCompany("Vector", 32);

		Employee e1 = service.createEmployee("Bob Dole", 210);
		service.setCompanyOfEmployee(e1, c2);
		Employee e2 = service.createEmployee("Alice Schmidt", 250);
		service.setCompanyOfEmployee(e2, c1);
		Employee e3 = service.createEmployee("George Down", 150);
		service.setCompanyOfEmployee(e3, c2);
		service.createEmployee("Rita Uphill", 300);
	}

}
