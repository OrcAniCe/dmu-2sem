package BST;

public class App {

	public static void main(String[] args) {
		BinarySearchTree<Integer> t = new BinarySearchTree<>();
		t.add(45);
		t.add(22);
		t.add(11);
		t.add(15);
		t.add(30);
		t.add(25);
		t.add(77);
		t.add(90);
		t.add(88);
		System.out.println("Find Max " + t.findMax());
		t.removeMin();
		t.print();
		System.out.println("Expected: 15 22 25 30 45 77 88 90");
	}

}
