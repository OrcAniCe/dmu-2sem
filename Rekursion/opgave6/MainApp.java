package opgave6;

public class MainApp {
	public static int Domino(int n) {
		if (n <= 3) {
			return n;
		} else {
			return Domino(n - 1) + Domino(n - 2);
		}
	}

	public static void main(String[] args) {
		System.out.println(Domino(3));
	}
}
