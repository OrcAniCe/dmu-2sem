package opgave2;

public class MainApp {

	public static int power(int n, int p) {
		if (p == 0) {
			return 1;
		} else {
			return n * power(n, p - 1);
		}
	}

	public static int power2(int n, int p) {
		if (p == 0) {
			return 1;
		} else if (p % 2 != 0) {
			return n * power2(n, p - 1);
		} else {
			return power2(n * n, p / 2);
		}
	}

	public static void main(String[] args) {
		System.out.println(power(5, 2));
		System.out.println(power2(5, 2));

	}

}
