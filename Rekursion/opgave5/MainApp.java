package opgave5;

public class MainApp {
	public static int Euclids(int a, int b) {
		if (b <= a && a % b == 0) {
			return b;
		} else if (a < b) {
			return Euclids(b, a);
		} else {
			return Euclids(b, a % b);
		}
	}

	public static void main(String[] args) {
		System.out.println(Euclids(2, 2));
	}

}
