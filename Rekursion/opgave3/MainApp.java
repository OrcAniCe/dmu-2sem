package opgave3;

public class MainApp {

	public static int product(int a, int b) {
		int result = 0;
		if (a * b == 0) {
			result = 0;
		} else if (a * b == b) {
			result = b;
		} else if (a > 1) {
			result = product(a - 1, b) + b;
		}
		return result;
	}

	public static int productRus(int a, int b) {
		int result = 0;
		if (a * b == 0) {
			result = 0;

		} else if (a % 2 != 0) {
			result = product(a - 1, b) + b;
		} else if (a > 1) {
			result = product(a / 2, 2 * b) + b;

		}
		return result;
	}

	public static void main(String[] args) {
		System.out.println(product(19, 3));
		System.out.println(productRus(19, 3));

	}

}
