package opgave1;

public class MainApp {

	public static int fac(int n) {
		int result = 0;
		if (n == 1) {
			result = 1;
		} else {
			result = n * fac(n - 1);
		}
		return result;
	}

	public static void test(int n) {
		if (n > 0) {
			System.out.println(n);
			test(n - 2);
			test(n - 3);
			System.out.println(n);
		}
	}

	public static void main(String[] args) {
		for (int i = 1; i <= 8; i++) {
			System.out.println("Den rekursive fak " + i + "  er  " + fac(i));
		}

		test(6);
		// TODO Auto-generated method stub

	}

}
