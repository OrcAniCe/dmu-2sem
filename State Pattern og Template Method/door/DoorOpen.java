package door;

public class DoorOpen extends DoorState {
	private Door door;

	public DoorOpen(Door door) {
		this.door = door;
	}

	@Override
	public String toString() {
		return "OPEN";
	}

	@Override
	public void click() {
		door.setState(door.getStayOpenState());
		door.startTimer(3000, event -> {
			door.setState(door.getOpenState());
		});
	}

}
