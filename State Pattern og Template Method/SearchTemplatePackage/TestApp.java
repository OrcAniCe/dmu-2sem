package SearchTemplatePackage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestApp {

	public static void main(String[] args) {

		List<String> list = new ArrayList<>();
		int size = 7;
		String[] list2 = new String[size];

		list.add("Mao");
		list.add("Hitler");
		list.add("Stalin");
		list.add("Lars Løkke");
		list.add("Anders Samuelsen");
		list.add("Kim Jong-Ill");
		list.add("Obama");

		for (int i = 0; i < list.size() - 1; i++) {
			list2[i] = list.get(i);
		}

		Arrays.sort(list2, 0, size - 1);

		SearchPattern searchableList = new SearchableList(list);
		SearchPattern searchableArray = new SearchableArray(list2);
		SearchPattern searchableBinary = new SearchableBinary(list2);

		System.out.println("searchable list\n");
		System.out.println(searchableList.search("Hitler"));
		System.out.println(searchableList.search("Rare Søren Pape"));
		System.out.println("searchable array\n");
		System.out.println(searchableArray.search("Hitler"));
		System.out.println(searchableArray.search("Rare Søren Pape"));
		System.out.println("searchable binary array\n");
		System.out.println(searchableBinary.search("Hitler"));
		System.out.println(searchableBinary.search("Rare Søren Pape"));
	}

}
