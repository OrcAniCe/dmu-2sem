package SearchTemplatePackage;

public class SearchableBinary<E extends Comparable<E>> extends SearchPattern<E> {

	private E[] list;
	private int index;
	private int left;
	private int right;
	private int middle;
	private int k;

	public SearchableBinary(E[] list) {
		this.list = list;
	}

	@Override
	protected void init() {
		index = 0;
		left = 0;
		right = list.length - 1;
		middle = -1;
	}

	@Override
	protected boolean isEmpty() {
		return index >= list.length;
	}

	@Override
	protected E select() {
		return list[k];
	}

	@Override
	protected void split(E m) {
		middle = (left + right) / 2;
		k = (int) list[middle];
		if (list[k].compareTo(m) > 0) {
			right = middle - 1;
		} else {
			left = middle + 1;
		}
	}
}
