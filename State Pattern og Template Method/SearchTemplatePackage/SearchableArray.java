package SearchTemplatePackage;

public class SearchableArray<E extends Comparable<E>> extends SearchPattern<E> {

	private E[] list;
	private int index;

	public SearchableArray(E[] list) {
		this.list = list;
	}

	@Override
	protected void init() {
		index = 0;
	}

	@Override
	protected boolean isEmpty() {
		return index >= list.length;
	}

	@Override
	protected E select() {
		return list[index];
	}

	@Override
	protected void split(E m) {
		index++;
	}
}
