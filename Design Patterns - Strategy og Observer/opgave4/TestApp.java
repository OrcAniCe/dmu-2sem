package opgave4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TestApp {

	public static void main(String[] args) {

		List<Customer> custList = new ArrayList<>();

		Customer c1 = new Customer("b", 3);

		c1.setComp(new NameComparator());

		Customer c2 = new Customer("e", 2);
		c2.setComp(new NameComparator());
		Customer c3 = new Customer("a", 4);
		c3.setComp(new NameComparator());
		Customer c4 = new Customer("h", 1);
		c4.setComp(new NameComparator());
		Customer c5 = new Customer("s", 8);
		c5.setComp(new NameComparator());
		Customer c6 = new Customer("u", 9);
		c6.setComp(new NameComparator());

		custList.add(c1);
		custList.add(c2);
		custList.add(c3);
		custList.add(c4);
		custList.add(c5);
		custList.add(c6);

		System.out.println(custList);
		Collections.sort(custList);
		System.out.println(custList);
	}
}
