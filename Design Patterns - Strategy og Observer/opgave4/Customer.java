package opgave4;
import java.util.Comparator;

public class Customer implements Comparable<Customer> {

	private String name;
	private int number;
	private Comparator<Customer> comp;

	public Customer(String name, int number) {
		this.name = name;
		this.number = number;

	}

	public void setComp(Comparator<Customer> comp) {
		this.comp = comp;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	@Override
	public String toString() {
		return name + " " + number;
	}

	@Override
	public int compareTo(Customer o) {
		return comp.compare(this, o);
	}
}