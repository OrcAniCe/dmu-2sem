package opgave5;

import java.util.HashSet;
import java.util.Set;

public class Saelger implements Observer {
	private String navn;

	public Saelger(String navn, Subject sub) {
		sub.addObserver(this);
		this.navn = navn;

	}

	@Override
	public void update(Subject s) {
		BogTitel bt = (BogTitel) s;
		Set<BogTitel> combinedBookList = new HashSet<>();
		for (Kunde k : bt.getKunder()) {
			for (BogTitel b : k.getBookList()) {

				combinedBookList.add(b);

			}
		}
		combinedBookList.remove(bt);

	}
}