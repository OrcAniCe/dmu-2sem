package opgave5;

public class TestApp {

	public static void main(String[] args) {
		BogTitel a = new BogTitel("Anders And", 6);
		BogTitel b = new BogTitel("Java", 8);
		BogTitel c = new BogTitel("Java1", 8);
		BogTitel d = new BogTitel("Java2", 8);

		Saelger s1 = new Saelger("Hansen", a);
		Indkoeber i1 = new Indkoeber("Jensen", a);

		a.addObserver(s1);
		b.addObserver(s1);
		c.addObserver(s1);
		d.addObserver(s1);

		a.addObserver(i1);
		b.addObserver(i1);
		c.addObserver(i1);
		d.addObserver(i1);

		Kunde k1 = new Kunde("laesehest1");
		Kunde k2 = new Kunde("laesehest2");
		Kunde k3 = new Kunde("laesehest3");

		a.etKoeb(k1);
		a.etKoeb(k2);
		a.etKoeb(k3);
		c.etKoeb(k1);
		c.etKoeb(k2);
		c.etKoeb(k3);
		d.etKoeb(k1);
		d.etKoeb(k2);
		d.etKoeb(k3);

		b.etKoeb(k1);
		b.etKoeb(k2);
		b.etKoeb(k3);

	}
}
