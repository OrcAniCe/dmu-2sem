package opgave5;

public class Indkoeber implements Observer {
	private String navn;

	public Indkoeber(String navn, Subject sub) {
		sub.addObserver(this);
		this.navn = navn;
	}

	@Override
	public void update(Subject s) {
		BogTitel bt = (BogTitel) s;
		if (bt.getAntal() < 6) {
			System.out.println("der købes 10 boger med titelen " + bt.getTitel());
			bt.indkoebTilLager(10);

		}
	}

}
