package opgave5;

import java.util.HashSet;
import java.util.Set;

public class BogTitel implements Subject {

	private String titel;
	private int antal;
	private Set<Observer> obsList = new HashSet<>();
	private Set<Kunde> kList = new HashSet<>();

	public BogTitel(String title, int antal) {
		this.antal = antal;
		this.titel = title;
	}

	public String getTitel() {
		return titel;
	}

	public int getAntal() {
		return antal;
	}

	public void indkoebTilLager(int antal) {
		this.antal = this.antal + antal;
	}

	public void etKoeb(Kunde k) {
		k.addBook(this);
		kList.add(k);
		System.out.print(k.getNavn() + " har købt: " + this.getTitel() + " -");
		this.antal--;
		System.out.println(" rest på lager: " + antal);

		for (Observer o : obsList) {
			o.update(this);
		}
	}

	public HashSet<Kunde> getKunder() {
		return new HashSet<Kunde>(kList);

	}

	@Override
	public void addObserver(Observer o) {
		obsList.add(o);

	}

	@Override
	public void removeObserver(Observer o) {
		obsList.remove(o);

	}

	public Set<Observer> getObs() {
		return new HashSet<Observer>(obsList);
	}

	@Override
	public String toString() {
		return titel;
	}

}