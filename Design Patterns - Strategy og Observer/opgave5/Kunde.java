package opgave5;

import java.util.HashSet;
import java.util.Set;

public class Kunde {
	private String navn;
	private Set<BogTitel> bookList = new HashSet<>();;

	public Kunde(String navn) {
		this.navn = navn;
	}

	public String getNavn() {
		return this.navn;
	}

	public Set<BogTitel> getBookList() {
		return new HashSet<BogTitel>(bookList);
	}

	public void addBook(BogTitel bt) {
		bookList.add(bt);
	}

	public void removeBook(BogTitel bt) {
		bookList.remove(bt);
	}

	@Override
	public String toString() {
		return navn;

	}

}
