package model;

import java.util.regex.Pattern;

public class EmailEvaluator implements Evaluator {

	private static final String formatSpecifier = "^[A-Z0-9._%+-]+[A-Z0-9]+@[A-Z0-9]+[A-Z0-9.-]+[A-Z]{1,}$";

	private static final Pattern formatToken = Pattern.compile(formatSpecifier, Pattern.CASE_INSENSITIVE);

	@Override
	public boolean isValid(String s) {

		if (!formatToken.matcher(s).matches()) {
			return false;
		} else {
			return true;
		}
	}

}
