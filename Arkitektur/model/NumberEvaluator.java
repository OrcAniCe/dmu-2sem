package model;

import java.util.regex.Pattern;

public class NumberEvaluator implements Evaluator {

	private static final String formatSpecifier = "^[0-9e.-]{1,}$";
	private static final Pattern formatToken = Pattern.compile(formatSpecifier, Pattern.CASE_INSENSITIVE);

	@Override
	public boolean isValid(String s) {

		if (formatToken.matcher(s).matches()) {
			try {
				Double.parseDouble(s);
			} catch (NumberFormatException e) {
				return false;
			}
			return true;
		}
		return false;
	}

}
