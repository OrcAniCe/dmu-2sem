package opg3;

import opg1.Stack;

public class DropOutStackArray<E, T> implements Stack<T> {

	private int topOfStack;
	int size;
	E[] arrayStack;

	public DropOutStackArray(int size) {
		this.size = size;
		this.arrayStack = (E[]) new Object[size];
		this.topOfStack = -1;
	}

	@Override
	public void push(T element) {
		dropOutResetter();
		topOfStack++;
		arrayStack[topOfStack] = (E) element;
	}

	@Override
	public E pop() {
		if (topOfStack >= 0) {
			E temp = arrayStack[topOfStack];
			arrayStack[topOfStack] = null;
			topOfStack--;
			return temp;
		} else {
			return null;
		}
	}

	@Override
	public E peek() {

		return arrayStack[topOfStack];

	}

	@Override
	public boolean isEmpty() {
		if (topOfStack == -1) {
			return true;
		} else {
			return false;
		}
	}

	private void dropOutResetter() {
		if (topOfStack == size - 1) {
			topOfStack = -1;
		}
	}

	public E[] getArray() {
		return arrayStack;
	}

}
