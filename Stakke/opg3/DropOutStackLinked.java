
package opg3;

import java.util.LinkedList;

import opg1.Stack;

public class DropOutStackLinked<E, T> implements Stack<T> {

	private int topOfStack;
	private boolean firstRun;
	private boolean popChecker;
	private int stackResetter;
	private int tempCounter;
	LinkedList<T> linkedStack;

	public DropOutStackLinked(int stackResetter) {
		this.topOfStack = 0;
		this.firstRun = true;
		this.popChecker = true;
		this.stackResetter = stackResetter;
		linkedStack = new LinkedList<>();
	}

	@Override
	public void push(T element) {
		if (topOfStack <= stackResetter - 1 && firstRun == true) {
			linkedStack.add(topOfStack, element);
			topOfStack++;
			if (topOfStack == stackResetter) {
				firstRun = false;
				topOfStack = 0;
			}
		} else {
			linkedStack.remove(topOfStack);
			linkedStack.add(topOfStack, element);
			topOfStack++;
			if (topOfStack == stackResetter) {
				topOfStack = 0;
			}

		}

	}

	public LinkedList<T> getStack() {
		return linkedStack;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T pop() {
		T value = linkedStack.remove(topOfStack);
		linkedStack.add(topOfStack, (T) " ");
		if (topOfStack == 0) {
			topOfStack = stackResetter;
		}
		topOfStack--;
		return value;
	}

	@Override
	public T peek() {
		return linkedStack.get(topOfStack);
	}

	@Override
	public boolean isEmpty() {
		return linkedStack.isEmpty();
	}

}
