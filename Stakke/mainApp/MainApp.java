package mainApp;

import java.util.Arrays;

import opg1.ArrayStack;
import opg2.PerenthesisChecker;
import opg3.DropOutStackLinked;

public class MainApp {

	public static void main(String[] args) {

		ArrayStack arrayStack = new ArrayStack(2);
		// DropOutStackArray dropOut = new DropOutStackArray(5);
		DropOutStackLinked dropOutSingleLinked = new DropOutStackLinked(5);
		PerenthesisChecker checker = new PerenthesisChecker();

		System.out.println(dropOutSingleLinked.isEmpty());

		dropOutSingleLinked.push("1");
		dropOutSingleLinked.push("2");
		dropOutSingleLinked.push("3");
		dropOutSingleLinked.push("4");
		dropOutSingleLinked.push("5");

		System.out.println(Arrays.toString(dropOutSingleLinked.getStack().toArray()));

		dropOutSingleLinked.push("6");
		System.out.println(Arrays.toString(dropOutSingleLinked.getStack().toArray()));
		dropOutSingleLinked.push("7");
		dropOutSingleLinked.push("8");
		System.out.println(Arrays.toString(dropOutSingleLinked.getStack().toArray()));
		dropOutSingleLinked.pop();
		dropOutSingleLinked.push("9");
		dropOutSingleLinked.push("10");
		System.out.println(Arrays.toString(dropOutSingleLinked.getStack().toArray()));
		dropOutSingleLinked.push("11");
		System.out.println(Arrays.toString(dropOutSingleLinked.getStack().toArray()));
		dropOutSingleLinked.pop();
		System.out.println(Arrays.toString(dropOutSingleLinked.getStack().toArray()));
		dropOutSingleLinked.pop();
		System.out.println(Arrays.toString(dropOutSingleLinked.getStack().toArray()));
		dropOutSingleLinked.pop();
		System.out.println(Arrays.toString(dropOutSingleLinked.getStack().toArray()));
		dropOutSingleLinked.pop();
		System.out.println(Arrays.toString(dropOutSingleLinked.getStack().toArray()));
		dropOutSingleLinked.pop();
		System.out.println(Arrays.toString(dropOutSingleLinked.getStack().toArray()));
		System.out.println(dropOutSingleLinked.isEmpty());

		// arrayStack.push("test");
		// arrayStack.push("test2");
		// System.out.println(Arrays.toString(arrayStack.getArray()));
		// arrayStack.push("test3");
		// arrayStack.push("test4");
		// System.out.println(Arrays.toString(arrayStack.getArray()));
		//
		// arrayStack.pop();
		// arrayStack.pop();
		// arrayStack.pop();
		// System.out.println(Arrays.toString(arrayStack.getArray()));
		//
		// arrayStack.pop();
		//
		// System.out.print("expected: false: ");
		// System.out.print(checker.CheckParentesis(")this[is]a{test{(") +
		// "\n");
		//
		// System.out.print("expected: false: ");
		// System.out.print(checker.CheckParentesis(")(") + "\n");
		//
		// System.out.print("expected: false: ");
		// System.out.print(checker.CheckParentesis("()(){}[)[") + "\n");
		//
		// System.out.print("expected: true: ");
		// System.out.print(checker.CheckParentesis("(this[is]a{test})") +
		// "\n");

	}

}
