package opg1;

public interface Stack<T> {

	public void push(T element);

	public Object pop();

	public Object peek();

	public boolean isEmpty();
}
