package opg1;

public class ArrayStack<E, T> implements Stack<T> {

	private int topOfStack;
	int size;
	E[] arrayStack;

	public ArrayStack(int size) {
		this.size = size;
		this.arrayStack = (E[]) new Object[size];
		this.topOfStack = -1;
	}

	@Override
	public void push(T element) {
		growIfNeccessary();
		topOfStack++;
		arrayStack[topOfStack] = (E) element;
	}

	@Override
	public E pop() {
		if (topOfStack >= 0) {
			E temp = arrayStack[topOfStack];
			arrayStack[topOfStack] = null;
			topOfStack--;
			return temp;
		} else {
			return null;
		}
	}

	@Override
	public E peek() {

		return arrayStack[topOfStack];

	}

	@Override
	public boolean isEmpty() {
		if (topOfStack == -1) {
			return true;
		} else {
			return false;
		}
	}

	private void growIfNeccessary() {
		int newSize;
		if (topOfStack == size - 1) {
			newSize = size * 2;
			E[] newArrayStack = (E[]) new Object[newSize];
			for (int i = 0; i < size; i++) {
				newArrayStack[i] = arrayStack[i];
			}
			arrayStack = newArrayStack;
		}
	}

	public E[] getArray() {
		return arrayStack;
	}

}
