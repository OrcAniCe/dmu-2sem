package opg2;

import opg1.ArrayStack;

public class PerenthesisChecker {

	public PerenthesisChecker() {

	}

	public static boolean CheckParentesis(String str) {
		if (str.isEmpty()) {
			return true;
		}

		char[] testArrayStack = str.toCharArray();
		ArrayStack testStack = new ArrayStack(testArrayStack.length);
		for (char c : testArrayStack) {
			if (c == '(' || c == '[' || c == '{') {
				testStack.push(c);
			}
			if (c == ')' || c == ']' || c == '}') {
				if (testStack.isEmpty()) {
					return false;
				}
				char last = (char) testStack.peek();
				if (c == '}' && last == '{' || c == ')' && last == '(' || c == ']' && last == '[') {
					testStack.pop();
				}
			}
		}
		return testStack.isEmpty();

	}

}
