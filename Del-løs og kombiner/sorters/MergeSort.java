package sorters;

import java.util.ArrayList;

public class MergeSort {

	public void mergeSort(ArrayList<Integer> list, int low, int high) {
		if (low < high) {
			int mid = (low + high) / 2;
			mergeSort(list, low, mid);
			mergeSort(list, mid + 1, high);
			merger(list, low, mid, high);
		}
	}

	private void merger(ArrayList<Integer> list, int low, int mid, int high) {

		ArrayList<Integer> mergedSortedArray = new ArrayList<>();

		int leftIndex = low;
		int rightIndex = mid + 1;

		while (leftIndex <= mid && rightIndex <= high) {
			if (list.get(leftIndex) <= list.get(rightIndex)) {
				mergedSortedArray.add(list.get(leftIndex));
				leftIndex++;
			} else {
				mergedSortedArray.add(list.get(rightIndex));
				rightIndex++;
			}
		}

		while (leftIndex <= mid) {
			mergedSortedArray.add(list.get(leftIndex));
			leftIndex++;
		}

		while (rightIndex <= high) {
			mergedSortedArray.add(list.get(rightIndex));
			rightIndex++;
		}

		int i = 0;
		int j = low;
		while (i < mergedSortedArray.size()) {
			list.set(j, mergedSortedArray.get(i++));
			j++;
		}
	}
}
