package sorters;

import java.util.ArrayList;
import java.util.Arrays;

public class TestApp {

	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();

		list.addAll(Arrays.asList(new Integer[] { 8, 56, 45, 34, 15, 12, 34, 44 }));
		System.out.println(list);

		MergeSort sort = new MergeSort();
		sort.mergeSort(list, 0, list.size() - 1);
		System.out.println();
		System.out.println(list);
	}

}
