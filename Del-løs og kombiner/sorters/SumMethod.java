package sorters;

import java.util.ArrayList;

public class SumMethod {

	private int numbOfZeroes = 0;

	public int sumInts(ArrayList<Integer> list, int low, int high) {
		if (low == high) {
			return list.get(low);
		} else {
			int middle = (low + high) / 2;

			int leftSum = sumInts(list, low, middle);
			int rightSum = sumInts(list, middle + 1, high);
			int result = leftSum + rightSum;
			return result;
		}
	}

	public int findZeroes(ArrayList<Integer> list, int low, int high) {
		if (low == high) {
			if (list.get(low) == 0) {
				numbOfZeroes++;
			}
			return numbOfZeroes;
		} else {
			int mid = (low + high) / 2;
			findZeroes(list, low, mid);
			findZeroes(list, mid + 1, high);
			return numbOfZeroes;
		}
	}

}
