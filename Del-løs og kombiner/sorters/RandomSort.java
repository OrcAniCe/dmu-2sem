package sorters;

import java.util.ArrayList;
import java.util.Random;

public class RandomSort {

	private static int c;
	private static ArrayList<Integer> intList = new ArrayList<>();
	private static Random rand = new Random();

	static SumMethod summethod = new SumMethod();
	static MergeSort mergeSort = new MergeSort();

	public static void main(String[] args) {

		while (c < 10)

		{
			intList.add(rand.nextInt(10));
			c++;
		}
		for (int i : intList) {
			System.out.print(i + " ");
		}

		mergeSort.mergeSort(intList, 0, c - 1);
		System.out.println();

		for (int i : intList) {
			System.out.print(i + " ");
		}

		// System.out.println("\n" + summethod.sumInts(intList, 0, c - 1));

		// System.out.println("\n" + summethod.findZeroes(intList, 0, c - 1));

	}
}
