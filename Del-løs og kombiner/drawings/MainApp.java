package drawings;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class MainApp extends Application {
	Canvas canvas;
	CanvasClass triangle = new CanvasClass();
	private static int height = 600;
	private static int width = 800;

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		GridPane root = initContent();
		Scene scene = new Scene(root);

		stage.setTitle("Loops");
		stage.setScene(scene);
		stage.show();
	}

	private GridPane initContent() {
		GridPane pane = new GridPane();
		canvas = new Canvas(600, 600);
		pane.add(canvas, 0, 0);
		triangle.drawTriangle(canvas.getGraphicsContext2D(), 100, 200, 200, 3);
		return pane;
	}

}
