package drawings;

import javafx.scene.canvas.GraphicsContext;

public class CanvasClass {
	int counter = 1;
	boolean initDraw;

	public void drawTriangle(GraphicsContext triangle, double x0, double y0, double len, int counter) {
		if (initDraw == true) {
			initDraw = false;
			strokeTriangle(triangle, x0, y0, len);
			drawTriangle(triangle, x0, y0, len * 0.5, counter);
		} else {
			while (counter > 0) {
				counter--;
				// strokeInvTriangle(triangle, x0, y0, len);
				// drawTriangle(triangle, x0, y0, len * 0.5, counter);
			}
		}
	}

	private double[] calcEndCords(double x0, double y0, double len, double ang) {
		double[] array = new double[2];
		array[0] = x0 + len * Math.cos(Math.toRadians(ang));
		array[1] = y0 - len * Math.sin(Math.toRadians(ang));

		return array;
	}

	private double calcHeight(double a, double b) {
		double height = Math.sqrt(Math.pow(a, 2) - Math.pow(b / 2, 2));
		return height;
	}

	private void strokeTriangle(GraphicsContext triangle, double x0, double y0, double len) {
		double[] arr = calcEndCords(x0, y0, len, 0);
		double[] arr1 = calcEndCords(arr[0], arr[1], len, 120);
		double[] arr2 = calcEndCords(x0, y0, len, 60);
		triangle.strokeLine(x0, y0, arr[0], arr[1]);
		triangle.strokeLine(arr[0], arr[1], arr1[0], arr1[1]);
		triangle.strokeLine(arr2[0], arr2[1], x0, y0);
	}

	private void strokeInvTriangle(GraphicsContext triangle, double x0, double y0, double len) {
		double[] arr = calcEndCords(x0, y0, len, 0);
		double[] arr1 = calcEndCords(arr[0], arr[1], len, 120);
		double[] arr2 = calcEndCords(x0, y0, len, 60);
		// triangle.strokeLine(x0 / 2, y0 / 2, arr[0] / 2, arr[1] / 2);
		triangle.strokeLine(arr[0] / 2, arr[1] / 2, arr1[0] / 2, arr1[1] / 2);
		triangle.strokeLine(arr2[0] / 2, arr2[1] / 2, x0 / 2, y0 / 2);
	}

}
