package studskole;

import java.util.ArrayList;
import java.util.List;

public class Skole {

	private String navn;
	List<Studerende> studList = new ArrayList<>();

	public Skole(String navn) {
		this.navn = navn;
	}

	public String getNavn() {
		return navn;
	}

	public void addStud(Studerende stud) {
		studList.add(stud);
	}

	public void removeStud(Studerende stud) {
		studList.remove(stud);
	}

	public double gennemsnit() {
		double result = 0;
		double sum = 0;
		int c = 0;
		for (Studerende s : studList) {
			for (int k : s.getKarakter()) {
				c++;
				sum += k;
			}
			result += sum / c;
		}

		return result / studList.size();
	}

	public Studerende findStud(int studNr) {

		for (Studerende s : studList) {
			if (s.getStudNr() == studNr) {
				return s;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return navn + studList;
	}

}
