package studskole;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class StudSkoleTest {
	// Studerende
	@Test
	public void testStudent() {
		Studerende student = new Studerende(00001, "Jane Jensen");
		assertNotNull(student);
		assertEquals("Jane Jensen", student.getNavn());
	}

	@Test
	public void testSetName() {
		Studerende student = new Studerende(00001, "Jane Jensen");
		student.setNavn("Hugo Mortensen");
		assertEquals("Hugo Mortensen", student.getNavn());
	}

	// Skole Tests
	// Tilføje elev til skole
	@Test
	public void testSetSkole() {
		Studerende student = new Studerende(00001, "Jane Jensen");
		Skole school = new Skole("Test Skole");
		school.addStud(student);
		assertTrue(school.findStud(00001) != null);
	}

}
