package library;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Test;

public class LibraryTest {
	Library dung = new Library();

	// TC1
	@Test(expected = RuntimeException.class)
	public void testDateIfBefore() {
		String dateUdlaan = "2015-01-15";
		String dateAktuel = "2015-01-10";
		dung.fineCalculator(LocalDate.parse(dateAktuel), LocalDate.parse(dateUdlaan), true);
	}

	// TC2
	@Test
	public void testFineOne() {
		String dateUdlaan = "2015-01-10";
		String dateAktuel = "2015-01-17";
		assertEquals(20, dung.fineCalculator(LocalDate.parse(dateAktuel), LocalDate.parse(dateUdlaan), true));
	}

	// TC3
	@Test
	public void testFineTwo() {
		String dateUdlaan = "2015-01-10";
		String dateAktuel = "2015-01-20";
		assertEquals(60, dung.fineCalculator(LocalDate.parse(dateAktuel), LocalDate.parse(dateUdlaan), true));
	}

	// TC4
	@Test
	public void testFineThree() {
		String dateUdlaan = "2015-01-10";
		String dateAktuel = "2015-02-14";
		assertEquals(90, dung.fineCalculator(LocalDate.parse(dateAktuel), LocalDate.parse(dateUdlaan), true));
	}

	// TC5
	@Test
	public void testFineFour() {
		String dateUdlaan = "2015-01-10";
		String dateAktuel = "2015-01-17";
		assertEquals(10, dung.fineCalculator(LocalDate.parse(dateAktuel), LocalDate.parse(dateUdlaan), false));
	}

	// TC6
	@Test
	public void testFineFive() {
		String dateUdlaan = "2015-01-10";
		String dateAktuel = "2015-01-20";
		assertEquals(30, dung.fineCalculator(LocalDate.parse(dateAktuel), LocalDate.parse(dateUdlaan), false));
	}

	// TC7
	@Test
	public void testFineSix() {
		String dateUdlaan = "2015-01-10";
		String dateAktuel = "2015-02-14";
		assertEquals(45, dung.fineCalculator(LocalDate.parse(dateAktuel), LocalDate.parse(dateUdlaan), false));
	}

	@Test
	// BEST IN TEST
	public void testForBeer() {
		boolean beer = true;
		assertTrue(beer);
	}

}
