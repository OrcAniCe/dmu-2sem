package bilforsikring;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BilforsikringTest {
	Bilforsikring bs = new Bilforsikring();

	@Test
	public void testSetPraemie() {
		bs.setGrundpræmie(10000);
		assertEquals(10000, bs.getGrundPræmie(), 0.000001);
	}

	@Test(expected = RuntimeException.class)
	public void testTooYoung() {
		bs.setGrundpræmie(10000);
		bs.beregnPræmie(17, true, 0);
	}

	@Test(expected = RuntimeException.class)
	public void testPraemieNotSet() {
		bs.setGrundpræmie(0);
		bs.getGrundPræmie();
	}

	@Test(expected = RuntimeException.class)
	public void testNegativeYears() {
		bs.setGrundpræmie(10000);
		bs.beregnPræmie(18, true, -1);
	}

	@Test(expected = RuntimeException.class)
	public void testNoAccidentTooGreat() {
		bs.setGrundpræmie(10000);
		bs.beregnPræmie(20, true, 8);
	}

	@Test
	public void testConfigurationOne() {
		bs.setGrundpræmie(10000);
		assertEquals(10000 * 1.25 * 0.95, bs.beregnPræmie(18, true, 0), 0.00001);
	}

	@Test
	public void testConfigurationTwo() {
		bs.setGrundpræmie(10000);
		assertEquals(10000 * 1.25, bs.beregnPræmie(18, false, 0), 0.00001);
	}

	@Test
	public void testConfigurationThree() {
		bs.setGrundpræmie(10000);
		assertEquals(10000, bs.beregnPræmie(25, false, 2), 0.00001);
	}

	@Test
	public void testConfigurationFour() {
		bs.setGrundpræmie(10000);
		assertEquals(10000 * 0.85, bs.beregnPræmie(25, false, 5), 0.00001);
	}

	@Test
	public void testConfigurationFive() {
		bs.setGrundpræmie(10000);
		assertEquals(10000 * 0.75, bs.beregnPræmie(26, false, 8), 0.00001);
	}

	@Test
	public void testConfigurationSix() {
		bs.setGrundpræmie(10000);
		assertEquals(10000 * 0.65, bs.beregnPræmie(30, false, 9), 0.00001);
	}

}