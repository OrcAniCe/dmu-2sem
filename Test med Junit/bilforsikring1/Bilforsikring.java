package bilforsikring1;

public class Bilforsikring {
	private double grundpræmie;

	public double getGrundPræmie() {
		return grundpræmie;
	}

	/**
	 * Beregner og returnerer en præmie udregnet ud fra følgende regler:
	 * grundPræmie danner udgangspunkt for præmien hvis der er tale om unge
	 * under 25 tillægges grundPræmien 25% hvis der er tale om en kvinde
	 * reduceres præmien med 5% hvis man har kørt skadefrit i: 0 til 2 år
	 * reduceres præmien med 0% 3 til 5 år reduceres præmien med 15% 6 til 8 år
	 * reduceres præmien med 25% over 8 år reduceres præmien med 35% ovenstående
	 * skal udregnes i den angivne rækkefølge
	 *
	 * Hvis parametrene ikke er indenfor det gyldige område kastes en exception
	 * med en passende tekst
	 *
	 * Krav: grundPræmie er tildelt værdi.
	 */

	public void setGrundpræmie(double grundPr) {
		if (grundPr <= 0) {
			throw new RuntimeException("grundPr skal vaere positiv");
		}
		grundpræmie = grundPr;
	}

	public double beregnPræmie(int alder, boolean isKvinde, int skadeFrieÅr) {
		double præmie = grundpræmie;
		if (præmie == 0) {
			throw new RuntimeException("GrundPræmie har ikke fået en værdi");
		}
		if (alder < 18) {
			throw new RuntimeException("Du er for ung til at tegne en forsikring");
		}
		if (alder - skadeFrieÅr < 18) {
			throw new RuntimeException("Du kan ikke have kaert skadefri sålænge");
		}
		if (skadeFrieÅr < 0) {
			throw new RuntimeException("Antal skade frie år skal være positiv");
		}

		if (alder < 25) {
			præmie = 1.25 * grundpræmie;
		}

		if (isKvinde) {
			præmie = præmie * 0.95;
		}

		if (skadeFrieÅr < 3) {
			;
		} else if (skadeFrieÅr < 6) {
			præmie = præmie * 0.85;
		} else if (skadeFrieÅr < 9) {
			præmie = præmie * 0.75;
		} else {
			præmie = præmie * 0.65;
		}
		return præmie;
	}

}
