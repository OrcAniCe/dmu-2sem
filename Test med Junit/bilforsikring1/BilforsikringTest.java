package bilforsikring1;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BilforsikringTest {

	private Bilforsikring bil = new Bilforsikring();

	// TC1 - grundPræmie 10000 - Alder 24 - Skadefri år 1 - isKvine = true
	@Test
	public void testBeregnPræmie1() {
		bil.setGrundpræmie(10000.0);
		assertEquals(11875, bil.beregnPræmie(24, true, 1), 0.000001);
	}

	// TC2 - grundPræmie 0 - Alder 24 - Skadefri år 1 - isKvine = true
	@Test(expected = RuntimeException.class)
	public void testBeregnPræmie2() {
		bil.setGrundpræmie(0.0);
		assertEquals(11875, bil.beregnPræmie(24, true, 1), 0.000001);
	}

	// TC3 - grundPræmie 10000 - Alder 17 - Skadefri år 0 - isKvine = true
	@Test(expected = RuntimeException.class)
	public void testBeregnPræmie3() {
		bil.setGrundpræmie(10000.0);
		assertEquals(11875, bil.beregnPræmie(17, true, 0), 0.000001);
	}

	// TC4 - grundPræmie 10000 - Alder 18 - Skadefri år 1 - isKvine = true
	@Test(expected = RuntimeException.class)
	public void testBeregnPræmie4() {
		bil.setGrundpræmie(10000.0);
		assertEquals(11875, bil.beregnPræmie(18, true, 1), 0.000001);
	}

	// TC5 - grundPræmie 10000 - Alder 18 - Skadefri år -1 - isKvine = true
	@Test(expected = RuntimeException.class)
	public void testBeregnPræmie5() {
		bil.setGrundpræmie(10000.0);
		assertEquals(11875, bil.beregnPræmie(18, true, -1), 0.000001);
	}

	// TC6 - grundPræmie 10000 - Alder 24 - Skadefri år 0 - isKvine = true
	@Test
	public void testBeregnPræmie6() {
		bil.setGrundpræmie(10000.0);
		assertEquals(11875, bil.beregnPræmie(24, true, 0), 0.000001);
	}

	// TC7 - grundPræmie 10000 - Alder 24 - Skadefri år 0 - isKvine = false
	@Test
	public void testBeregnPræmie7() {
		bil.setGrundpræmie(10000.0);
		assertEquals(12500, bil.beregnPræmie(24, false, 0), 0.000001);
	}

	// TC8 - grundPræmie 10000 - Alder 25 - Skadefri år 0 - isKvine = true
	@Test
	public void testBeregnPræmie8() {
		bil.setGrundpræmie(10000.0);
		assertEquals(9500, bil.beregnPræmie(25, true, 0), 0.000001);
	}

	// TC9 - grundPræmie 10000 - Alder 25 - Skadefri år 0 - isKvine = false
	@Test
	public void testBeregnPræmie9() {
		bil.setGrundpræmie(10000.0);
		assertEquals(10000, bil.beregnPræmie(25, false, 0), 0.000001);
	}

	// TC10 - grundPræmie 10000 - Alder 25 - Skadefri år 2 - isKvine = false
	@Test
	public void testBeregnPræmie10() {
		bil.setGrundpræmie(10000.0);
		assertEquals(10000, bil.beregnPræmie(25, false, 2), 0.000001);
	}

	// TC11 - grundPræmie 10000 - Alder 25 - Skadefri år 5 - isKvine = false
	@Test
	public void testBeregnPræmie11() {
		bil.setGrundpræmie(10000.0);
		assertEquals(8500, bil.beregnPræmie(25, false, 5), 0.000001);
	}

	// TC12 - grundPræmie 10000 - Alder 25 - Skadefri år 6 - isKvine = false
	@Test
	public void testBeregnPræmie12() {
		bil.setGrundpræmie(10000.0);
		assertEquals(7500, bil.beregnPræmie(25, false, 6), 0.000001);
	}

	// TC13 - grundPræmie 10000 - Alder 35 - Skadefri år 8 - isKvine = false
	@Test
	public void testBeregnPræmie13() {
		bil.setGrundpræmie(10000.0);
		assertEquals(7500, bil.beregnPræmie(35, false, 8), 0.000001);
	}

	// TC14 - grundPræmie 10000 - Alder 35 - Skadefri år 9 - isKvine = false
	@Test
	public void testBeregnPræmie14() {
		bil.setGrundpræmie(10000.0);
		assertEquals(6500, bil.beregnPræmie(35, false, 9), 0.000001);
	}
}