package bibliotek;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Bibliotek {

	/*
	 * Returnerer størrelsen af bøden beregnet i henhold til skemaet ovenfor
	 * krav: beregnetDato < faktiskDato (beregnetDato er forventet
	 * afleveringsdato og faktiskDato er den dag bogen blev afleveret; voksen er
	 * sand, hvis det er en voksen og falsk ellers)
	 */
	public int fineCalculator(LocalDate beregnetDato, LocalDate faktiskDato, boolean voksen) {
		int fine = 0;
		if (beregnetDato.isBefore(faktiskDato)) {
			throw new RuntimeException("Din beregnede dato kan ikke være i fortiden.");
		}
		if (ChronoUnit.DAYS.between(faktiskDato, beregnetDato) == 0) {
			fine = 0;
		} else if (ChronoUnit.DAYS.between(faktiskDato, beregnetDato) <= 7
				&& ChronoUnit.DAYS.between(faktiskDato, beregnetDato) >= 1) {
			if (voksen == true) {
				fine = 20;
			} else {
				fine = 10;
			}
		} else if (ChronoUnit.DAYS.between(faktiskDato, beregnetDato) <= 14
				&& ChronoUnit.DAYS.between(faktiskDato, beregnetDato) >= 8) {
			if (voksen == true) {
				fine = 60;
			} else {
				fine = 30;
			}
		} else if (ChronoUnit.DAYS.between(faktiskDato, beregnetDato) >= 15) {
			if (voksen == true) {
				fine = 90;
			} else {
				fine = 45;
			}
		}
		return fine;
	}
}
