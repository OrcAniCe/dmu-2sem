package semesterprøve_2015_01;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class Kamp implements Comparable<Kamp> {
	private String sted;
	private LocalDate dato;
	private LocalTime tid;
	private ArrayList<Deltagelse> deltagelser = new ArrayList<Deltagelse>();

	public Kamp(String sted, LocalDate dato, LocalTime tid) {
		this.sted = sted;
		this.dato = dato;
		this.tid = tid;
	}

	public String getSted() {
		return sted;
	}

	public void setSted(String sted) {
		this.sted = sted;
	}

	public LocalDate getDato() {
		return dato;
	}

	public void setDato(LocalDate dato) {
		this.dato = dato;
	}

	public LocalTime getTid() {
		return tid;
	}

	public void setTid(LocalTime tid) {
		this.tid = tid;
	}

	public ArrayList<Deltagelse> getDeltagelser() {
		return deltagelser;
	}

	public void addDeltagelse(Deltagelse d) {
		this.deltagelser.add(d);
	}

	public void removeDeltagelse(Deltagelse d) {
		this.deltagelser.remove(d);
	}

	public ArrayList<String> afbud() {
		ArrayList<String> afbudsliste = new ArrayList<String>();

		for (int i = 0; i < deltagelser.size(); i++) {
			if (deltagelser.get(i).getAfbud()) {
				afbudsliste.add("Spiller: " + deltagelser.get(i).getSpiller() + " Begrundelse: "
						+ deltagelser.get(i).getBegrundelse());
			}
		}
		return afbudsliste;
	}

	@Override
	public int compareTo(Kamp o) {
		if (this.getDato() == o.getDato()) {
			if (this.getTid() == this.getTid()) {
				return this.getSted().compareTo(o.getSted());
			} else if (this.getTid().isBefore(o.getTid())) {
				return -1;
			} else {
				return 1;
			}
		} else if (this.getDato().isBefore(o.getDato())) {
			return -1;
		} else {
			return 1;
		}
	}

	public void spillerHonorar(String tekstFil) {
		try {
			PrintWriter out = new PrintWriter(tekstFil + ".txt");
			for (int i = 0; i < deltagelser.size(); i++) {
				if (!deltagelser.get(i).getAfbud()) {
					out.printf("%10s%10s%n", "Spiller: " + deltagelser.get(i).getSpiller().getNavn(),
							" Honorar: " + deltagelser.get(i).getSpiller().kampHonorar());
				}
			}
			out.close();
		} catch (FileNotFoundException e) {
			System.out.println("File not found.");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@Override
	public String toString() {
		return sted + " " + dato + " " + tid;
	}

}