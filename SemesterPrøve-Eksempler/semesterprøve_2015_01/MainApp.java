package semesterprøve_2015_01;

import java.time.LocalDate;
import java.time.LocalTime;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class MainApp extends Application {
	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Administration af spiller deltagelse");
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane, 600, 250);
		stage.setScene(scene);
		stage.show();
	}

	// -------------------------------------------------------------------------

	private ListView<Kamp> lvwKampe;
	private TextField txfSpillested;
	private TextField txfSpilledato;
	private TextField txfSpilletid;
	private Button btnOpret;
	private Button btnOpdater;
	private Button btnLavFil;

	private final Controller controller = new Controller();

	private void initContent(GridPane pane) {
		// show or hide grid lines
		pane.setGridLinesVisible(false);
		// set padding of the pane
		pane.setPadding(new Insets(20));
		// set horizontal gap between components
		pane.setHgap(10);
		// set vertical gap between components
		pane.setVgap(10);

		// add labels to the pane
		Label lblKampe = new Label("Kampe");
		pane.add(lblKampe, 0, 0);
		Label lblSpillested = new Label("Spillested:");
		pane.add(lblSpillested, 1, 0);
		Label lblSpilledato = new Label("Spilledato:");
		pane.add(lblSpilledato, 1, 1);
		Label lblSpilletid = new Label("Spilletid:");
		pane.add(lblSpilletid, 1, 2);

		// add a listView to the pane(at col=1, row=0)
		lvwKampe = new ListView<Kamp>();
		pane.add(lvwKampe, 0, 1, 1, 5);
		lvwKampe.getItems().setAll(Service.getKampe());

		ChangeListener<Kamp> listener = (ov, oldKamp, newKamp) -> this.controller.selectionChanged();
		lvwKampe.getSelectionModel().selectedItemProperty().addListener(listener);
		lvwKampe.getSelectionModel().clearSelection();

		// add a text field to the pane (at col=6, row=0, spanning 2 columns and1 row)
		txfSpillested = new TextField();
		pane.add(txfSpillested, 2, 0, 2, 1);
		txfSpilledato = new TextField();
		pane.add(txfSpilledato, 2, 1, 2, 1);
		txfSpilletid = new TextField();
		pane.add(txfSpilletid, 2, 2, 2, 1);

		// initialize and add buttons to the pane
		btnOpret = new Button("Opret");
		pane.add(btnOpret, 2, 3);
		btnOpdater = new Button("Opdater");
		pane.add(btnOpdater, 3, 3);
		btnLavFil = new Button("Lav fil");
		pane.add(btnLavFil, 2, 4);

		// Button actions
		btnOpret.setOnAction(event -> controller.opretKampAction());
		btnOpdater.setOnAction(event -> controller.opdaterKampAction());
		btnLavFil.setOnAction(event -> controller.lavFilAction());
	}

	// -------------------------------------------------------------------------

	private class Controller {

		public Controller() {
			Service.createSomeObjects();
		}

		public void opretKampAction() {
			try {
				String sted = "";
				LocalDate dato = null;
				LocalTime tid = null;

				if (!txfSpillested.getText().isEmpty()) {
					sted = txfSpillested.getText();
				}
				if (txfSpilledato != null) {
					dato = LocalDate.parse(txfSpilledato.getText());
				}
				if (txfSpilletid != null) {
					tid = LocalTime.parse(txfSpilletid.getText());
				}
				Service.createKamp(sted, dato, tid);
				lvwKampe.getItems().setAll(Service.getKampe());
				txfSpillested.clear();
				txfSpilledato.clear();
				txfSpilletid.clear();
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}

		public void opdaterKampAction() {
			try {
				Kamp kamp = lvwKampe.getSelectionModel().getSelectedItem();
				if (kamp != null) {
					if (!txfSpillested.getText().isEmpty() && !txfSpilledato.getText().isEmpty()
							&& !txfSpilletid.getText().isEmpty()) {
						kamp.setSted(txfSpillested.getText());
						kamp.setDato(LocalDate.parse(txfSpilledato.getText()));
						kamp.setTid(LocalTime.parse(txfSpilletid.getText()));
						lvwKampe.getItems().setAll(Service.getKampe());
						txfSpillested.clear();
						txfSpilledato.clear();
						txfSpilletid.clear();
					}
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}

		public void lavFilAction() {
			try {
				Kamp kamp = lvwKampe.getSelectionModel().getSelectedItem();
				String tekstFil = "spillerhonorar_" + kamp.getSted() + "_" + kamp.getDato();
				Service.spillerHonorar(kamp, tekstFil);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}

		// -------------------------------------------------------------------------
		// Selectionchange actions

		private void selectionChanged() {
			Kamp kampSelection = lvwKampe.getSelectionModel().getSelectedItem();
			if (kampSelection != null) {
				txfSpillested.setText(kampSelection.getSted());
				txfSpilledato.setText(kampSelection.getDato().toString());
				txfSpilletid.setText(kampSelection.getTid().toString());
			} else {
				txfSpillested.clear();
				txfSpilledato.clear();
				txfSpilletid.clear();
			}
		}
	}
}
