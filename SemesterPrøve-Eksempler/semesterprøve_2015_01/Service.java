package semesterprøve_2015_01;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;

public class Service {

	public static Kamp createKamp(String sted, LocalDate dato, LocalTime tid) {
		Kamp kamp = new Kamp(sted, dato, tid);
		Storage.addKamp(kamp);
		return kamp;
	}

	public static Spiller createSpiller(String navn, int årgang) {
		Spiller spiller = new Spiller(navn, årgang);
		Storage.addSpiller(spiller);
		return spiller;
	}

	public static ProfSpiller createProfSpiller(String navn, int årgang, double kampHonorar) {
		ProfSpiller profSpiller = new ProfSpiller(navn, årgang, kampHonorar);
		Storage.addSpiller(profSpiller);
		return profSpiller;
	}

	public static Deltagelse createDeltagelse(Kamp kamp, boolean afbud, String begrundelse, Spiller spiller) {
		Deltagelse deltagelse = new Deltagelse(afbud, begrundelse, spiller);
		kamp.addDeltagelse(deltagelse);
		spiller.addDeltagelse(deltagelse);
		return deltagelse;
	}

	public static ArrayList<Kamp> getKampe() {
		return new ArrayList<Kamp>(Storage.getKampe());
	}

	public static void spillerHonorar(Kamp kamp, String tekstFil) {
		kamp.spillerHonorar(tekstFil);
	}

	/**
	 * Opdaterer sammenhængen mellem spiller og deltagelse så de linker til
	 * hinanden Precondition: spiller != null og deltagelse != null
	 */
	public static void updateSpillerDeltagelse(Spiller spiller, Deltagelse deltagelse) {
		if (spiller != null && deltagelse != null) {
			spiller.addDeltagelse(deltagelse);
			deltagelse.setSpiller(spiller);
		}
	}

	public static ArrayList<Kamp> alleKampe(ArrayList<Kamp> list1, ArrayList<Kamp> list2) {
		Collections.sort(list1);
		Collections.sort(list2);
		ArrayList<Kamp> alleKampe = new ArrayList<Kamp>();
		int i = 0;
		int j = 0;
		while (i < list1.size() && j < list2.size()) {
			Kamp e1 = list1.get(i);
			Kamp e2 = list2.get(j);
			if (e1.compareTo(e2) <= 0) {
				alleKampe.add(e1);
				i++;
			} else {
				alleKampe.add(e2);
				j++;
			}
		}
		while (i != list1.size()) {
			Kamp e1 = list1.get(i);
			alleKampe.add(e1);
			i++;
		}

		while (j != list2.size()) {
			Kamp e2 = list2.get(j);
			alleKampe.add(e2);
			j++;
		}
		return alleKampe;
	}

	public static void createSomeObjects() {
		Spiller s1 = createSpiller("Jane Jensen", 1999);
		Spiller s2 = createSpiller("Lene Hansen", 2000);
		Spiller s3 = createSpiller("Mette Petersen", 1999);
		ProfSpiller s4 = createProfSpiller("Sofia Kjeldsen", 1999, 50.0);
		ProfSpiller s5 = createProfSpiller("Maria Nielsen", 2000, 55.0);

		Kamp k1 = createKamp("Herning", LocalDate.of(2015, 01, 26), LocalTime.of(10, 30));
		Kamp k2 = createKamp("Ikast", LocalDate.of(2015, 01, 27), LocalTime.of(13, 30));

		createDeltagelse(k1, true, "Fødselsdag hos Moster Oda", s1);
		createDeltagelse(k2, false, null, s1);
		createDeltagelse(k1, false, "", s2);
		createDeltagelse(k2, false, "", s2);
		createDeltagelse(k1, false, "", s3);
		createDeltagelse(k2, false, "", s3);
		createDeltagelse(k1, false, "", s4);
		createDeltagelse(k2, true, "Dårlig form", s4);
		createDeltagelse(k1, false, "", s5);
		createDeltagelse(k2, false, "", s5);
	}
}
