package semesterprøve_2015_01;

import java.util.ArrayList;

public class Storage {
	private static ArrayList<Kamp> kampe = new ArrayList<Kamp>();
	private static ArrayList<Spiller> spillere = new ArrayList<Spiller>();

	public static ArrayList<Kamp> getKampe() {
		return kampe;
	}

	public static void addKamp(Kamp kamp) {
		kampe.add(kamp);
	}

	public static ArrayList<Spiller> getSpillere() {
		return spillere;
	}

	public static void addSpiller(Spiller spiller) {
		spillere.add(spiller);
	}
}
