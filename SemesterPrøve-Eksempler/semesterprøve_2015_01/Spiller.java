package semesterprøve_2015_01;

import java.util.ArrayList;

public class Spiller {
	private String navn;
	private int årgang;
	private ArrayList<Deltagelse> deltagelser = new ArrayList<Deltagelse>();

	public Spiller(String navn, int årgang) {
		this.navn = navn;
		this.årgang = årgang;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public int getÅrgang() {
		return årgang;
	}

	public void setÅrgang(int årgang) {
		this.årgang = årgang;
	}

	public ArrayList<Deltagelse> getDeltagelser() {
		return deltagelser;
	}

	public void addDeltagelse(Deltagelse deltagelse) {
		deltagelser.add(deltagelse);
	}

	public void removeDeltagelse(Deltagelse deltagelse) {
		deltagelser.remove(deltagelse);
	}

	public double kampHonorar() {
		int antalDeltagelser = 0;
		for (int i = 0; i < deltagelser.size(); i++) {
			if (!deltagelser.get(i).getAfbud()) {
				antalDeltagelser++;
			}
		}
		return antalDeltagelser * 10.0;
	}
}
