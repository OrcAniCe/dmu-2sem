package semesterprøve_2015_01;

public class ProfSpiller extends Spiller {
	private double kampHonorar;

	public ProfSpiller(String navn, int årgang, double kampHonorar) {
		super(navn, årgang);
		this.kampHonorar = kampHonorar;
	}

	public double getKampHonorar() {
		return kampHonorar;
	}

	public void setKampHonorar(double kampHonorar) {
		this.kampHonorar = kampHonorar;
	}

	@Override
	public double kampHonorar() {
		int antalAfbud = 0;
		double afbudProcent = 0;
		for (int i = 0; i < getDeltagelser().size(); i++) {
			if (getDeltagelser().get(i).getAfbud()) {
				antalAfbud++;
			}
		}
		afbudProcent = antalAfbud / getDeltagelser().size();
		return (1 - afbudProcent) * kampHonorar;
	}

}
