package semesterprøve_2015_06;

import java.util.ArrayList;

public class Parkeringshus {
	private String adresse;
	private ArrayList<Parkeringsplads> parkeringspladser = new ArrayList<Parkeringsplads>();
	private double saldo;

	public Parkeringshus(String adresse) {
		this.adresse = adresse;
	}

	public String getAdresse() {
		return adresse;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = this.saldo + saldo;
	}

	public ArrayList<Parkeringsplads> getParkeringspladser() {
		return parkeringspladser;
	}

	public void addParkeringsplads(Parkeringsplads p) {
		this.parkeringspladser.add(p);
	}

	public void removeParkeringsplads(Parkeringsplads p) {
		this.parkeringspladser.remove(p);
	}

	public Parkeringsplads createParkeringsplads(Parkeringshus parkeringshus) {
		Parkeringsplads p = new Parkeringsplads(this);
		this.parkeringspladser.add(p);
		return p;
	}

	public Invalideplads createInvalideplads(Parkeringshus parkeringshus, double areal) {
		Invalideplads p = new Invalideplads(this, areal);
		this.parkeringspladser.add(p);
		return p;
	}

	public int antalLedigePladser() {
		int antalLedigePladser = 0;
		for (int i = 0; i < parkeringspladser.size(); i++) {
			if (parkeringspladser.get(i).getAnkomst() == null) {
				antalLedigePladser++;
			}
		}
		return antalLedigePladser;
	}

	public int findPladsMedBil(String regNummer) {
		boolean found = false;
		int i = 0;
		while (!found && i < parkeringspladser.size()) {
			if (parkeringspladser.get(i).getBil().getRegNr().equals(regNummer)) {
				found = true;
			} else {
				i++;
			}
		}
		if (found) {
			return parkeringspladser.get(i).getNummer();
		} else {
			return -1;
		}
	}

	public int findAntalBiler(Bilmærke mærke) {
		int antalBiler = 0;
		for (int i = 0; i < parkeringspladser.size(); i++) {
			if (parkeringspladser.get(i).getBil().getMærke().equals(mærke)) {
				antalBiler++;
			}
		}
		return antalBiler;
	}

	public ArrayList<String> optagnePladser() {
		ArrayList<String> optagnePladser = new ArrayList<String>();
		for (Parkeringsplads p : parkeringspladser) {
			if (p.getBil() != null) {
				optagnePladser
						.add("Plads " + p.getNummer() + ": " + p.getBil().getRegNr() + ", " + p.getBil().getMærke());
			}
		}
		return optagnePladser;
	}

	@Override
	public String toString() {
		return adresse;
	}
}
