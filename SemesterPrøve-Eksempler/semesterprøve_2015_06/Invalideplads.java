package semesterprøve_2015_06;

import java.time.LocalTime;

public class Invalideplads extends Parkeringsplads {
	private double areal;

	public Invalideplads(Parkeringshus parkeringshus, double areal) {
		super(parkeringshus);
		this.areal = areal;
	}

	public double getAreal() {
		return areal;
	}

	@Override
	public int beregnParkeringspris(LocalTime afhentning) {
		return 0;
	}

}
