package semesterprøve_2015_06;

import java.util.ArrayList;

public class Storage {
	private static ArrayList<Parkeringshus> parkeringshuse = new ArrayList<Parkeringshus>();
	private static ArrayList<Bil> biler = new ArrayList<Bil>();

	public static ArrayList<Parkeringshus> getParkeringshuse() {
		return parkeringshuse;
	}

	public static void addParkeringshus(Parkeringshus parkeringshus) {
		parkeringshuse.add(parkeringshus);
	}

	public static ArrayList<Bil> getBiler() {
		return biler;
	}

	public static void addBil(Bil bil) {
		biler.add(bil);
	}
}
