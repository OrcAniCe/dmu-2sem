package semesterprøve_2015_06;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class MainApp extends Application {
	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Parkeringshuse og pladser");
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane, 600, 400);
		stage.setScene(scene);
		stage.show();
	}

	// -------------------------------------------------------------------------

	private ListView<Parkeringshus> lvwParkeringshuse;
	private ListView<String> lvwOptagnePladser;
	private TextField txfRegNummer;
	private Button btnOpret;

	private final Controller controller = new Controller();

	private void initContent(GridPane pane) {
		// show or hide grid lines
		pane.setGridLinesVisible(false);
		// set padding of the pane
		pane.setPadding(new Insets(20));
		// set horizontal gap between components
		pane.setHgap(10);
		// set vertical gap between components
		pane.setVgap(10);

		// add labels to the pane
		Label lblParkeringshuse = new Label("Parkeringshuse");
		pane.add(lblParkeringshuse, 0, 0);
		Label lblOptagnePladser = new Label("Optagne pladser");
		pane.add(lblOptagnePladser, 1, 0);
		Label lblRegNr = new Label("Opret bil med nummer:");
		pane.add(lblRegNr, 0, 2);

		// add a listView to the pane(at col=1, row=0)
		lvwParkeringshuse = new ListView<Parkeringshus>();
		pane.add(lvwParkeringshuse, 0, 1);
		lvwParkeringshuse.getItems().setAll(Service.getParkeringshuse());

		lvwOptagnePladser = new ListView<String>();
		pane.add(lvwOptagnePladser, 1, 1);

		ChangeListener<Parkeringshus> listener = (ov, oldParkeringshus, newParkeringshus) -> this.controller
				.selectionChanged();
		lvwParkeringshuse.getSelectionModel().selectedItemProperty().addListener(listener);
		lvwParkeringshuse.getSelectionModel().clearSelection();

		// add a text field to the pane
		txfRegNummer = new TextField();
		pane.add(txfRegNummer, 1, 2);

		// initialize and add buttons to the pane
		btnOpret = new Button("Opret");
		pane.add(btnOpret, 1, 3);

		// Button actions
		btnOpret.setOnAction(event -> controller.opretBilAction());
	}

	// -------------------------------------------------------------------------

	private class Controller {

		public Controller() {
			Service.createSomeObjects();
		}

		public void opretBilAction() {
			try {
				String regNr = "";

				if (!txfRegNummer.getText().isEmpty()) {
					regNr = txfRegNummer.getText();
					Parkeringshus parkeringshusSelection = lvwParkeringshuse.getSelectionModel().getSelectedItem();
					if (parkeringshusSelection != null && parkeringshusSelection.antalLedigePladser() != 0) {
						Bil b = Service.createBil(regNr);
						for (int i = 0; i < parkeringshusSelection.getParkeringspladser().size(); i++) {
							if (parkeringshusSelection.getParkeringspladser().get(i).getBil() == null) {
								parkeringshusSelection.getParkeringspladser().get(i).setBil(b);
								break;
							}
						}
						lvwOptagnePladser.getItems().setAll(parkeringshusSelection.optagnePladser());
						txfRegNummer.clear();
					} else {
						Alert alert = new Alert(AlertType.INFORMATION);
						alert.setTitle("Information");
						alert.setHeaderText("Ingen ledige pladser");
						alert.setContentText(parkeringshusSelection + " har ingen ledige pladser.");
						alert.show();
						txfRegNummer.clear();
					}
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}

		// -------------------------------------------------------------------------
		// Selectionchange actions

		private void selectionChanged() {
			lvwOptagnePladser.getItems().clear();
			Parkeringshus parkeringshusSelection = lvwParkeringshuse.getSelectionModel().getSelectedItem();
			if (parkeringshusSelection != null) {
				lvwOptagnePladser.getItems().setAll(parkeringshusSelection.optagnePladser());
			} else {
				txfRegNummer.clear();
			}
		}
	}
}
