package semesterprøve_2015_06;

public class Bil {
	private String regNr;
	private Bilmærke mærke;

	public Bil(String regNr) {
		this.regNr = regNr;
	}

	public String getRegNr() {
		return regNr;
	}

	public Bilmærke getMærke() {
		return mærke;
	}

	public void setMærke(Bilmærke mærke) {
		this.mærke = mærke;
	}

}
