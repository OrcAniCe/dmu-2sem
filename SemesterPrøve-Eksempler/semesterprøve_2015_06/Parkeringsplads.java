package semesterprøve_2015_06;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class Parkeringsplads {
	private int nummer;
	private LocalTime ankomst = null;
	private Parkeringshus parkeringshus;
	private Bil bil;
	private static int lastAssignednummer = 0;

	public Parkeringsplads(Parkeringshus parkeringshus) {
		lastAssignednummer++;
		this.nummer = lastAssignednummer;
	}

	public int getNummer() {
		return nummer;
	}

	public LocalTime getAnkomst() {
		return ankomst;
	}

	public Parkeringshus getParkeringshus() {
		return parkeringshus;
	}

	public Bil getBil() {
		return bil;
	}

	public void setBil(Bil b) {
		this.ankomst = LocalTime.now();
		this.bil = b;
	}

	public int beregnParkeringspris(LocalTime afgangstidpunkt) {
		int pris = 0;
		double parkeringsMinutter = 0;
		double parkeringsEnheder = 0.0;
		if (this.bil != null) {
			parkeringsMinutter = ChronoUnit.MINUTES.between(ankomst, afgangstidpunkt);
			parkeringsEnheder = parkeringsMinutter / 10;
			if (parkeringsEnheder % 10 != 0) {
				parkeringsEnheder = (int) parkeringsEnheder + 1;
			}
			pris = (int) parkeringsEnheder * 6;
		}
		return pris;
	}

	public void hentBil(LocalTime afgangstidpunkt) {
		this.parkeringshus.setSaldo(beregnParkeringspris(afgangstidpunkt));
		this.ankomst = null;
		this.bil = null;
	}

}
