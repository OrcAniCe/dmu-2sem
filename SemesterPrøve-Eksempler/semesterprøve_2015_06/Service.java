package semesterprøve_2015_06;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Service {

	public static Parkeringshus createParkeringshus(String adresse) {
		Parkeringshus parkeringshus = new Parkeringshus(adresse);
		Storage.addParkeringshus(parkeringshus);
		return parkeringshus;
	}

	public static Parkeringsplads createParkeringsplads(Parkeringshus parkeringshus) {
		Parkeringsplads parkeringsplads = parkeringshus.createParkeringsplads(parkeringshus);
		return parkeringsplads;
	}

	public static Invalideplads createInvalideplads(Parkeringshus parkeringshus, double areal) {
		Invalideplads invalideplads = parkeringshus.createInvalideplads(parkeringshus, areal);
		return invalideplads;
	}

	public static Bil createBil(String regNr) {
		Bil bil = new Bil(regNr);
		return bil;
	}

	public static ArrayList<Parkeringshus> getParkeringshuse() {
		return new ArrayList<Parkeringshus>(Storage.getParkeringshuse());
	}

	public static void createSomeObjects() {
		Bil b1 = createBil("AB 11 222");
		b1.setMærke(Bilmærke.AUDI);
		Bil b2 = createBil("EF 33 444");
		b2.setMærke(Bilmærke.AUDI);
		Bil b3 = createBil("BD 55 666");
		b3.setMærke(Bilmærke.SKODA);

		Parkeringshus p = createParkeringshus("Havnegade 12, 8000 Aarhus");

		for (int i = 1; i <= 100; i++) {
			createParkeringsplads(p);
		}

		for (int j = 101; j <= 110; j++) {
			createInvalideplads(p, 18);
		}

		ArrayList<Parkeringsplads> parkeringspladser = p.getParkeringspladser();
		parkeringspladser.get(2).setBil(b1);
		parkeringspladser.get(4).setBil(b2);
		parkeringspladser.get(100).setBil(b3);
	}

	public static void writeOptagnePladser(Parkeringshus hus, String filnavn) {
		try {
			PrintWriter out = new PrintWriter(hus + "_" + filnavn + ".txt");
			for (Parkeringsplads p : hus.getParkeringspladser()) {
				if (p.getBil() != null) {
					out.println("Plads " + p.getNummer() + ": " + p.getBil().getRegNr() + ", " + p.getBil().getMærke());
				}
			}
			out.close();
		} catch (FileNotFoundException e) {
			System.out.println("File not found.");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
