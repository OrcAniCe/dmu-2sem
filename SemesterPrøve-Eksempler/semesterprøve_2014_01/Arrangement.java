package semesterprøve_2014_01;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class Arrangement {
	private String navn;
	private String ansvarlig;
	private LocalDate premiereDato;
	private int pris;
	private ArrayList<Event> events = new ArrayList<Event>();

	public Arrangement(String navn, String ansvarlig, LocalDate premiereDato, int pris) {
		this.navn = navn;
		this.ansvarlig = ansvarlig;
		this.premiereDato = premiereDato;
		this.pris = pris;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public String getAnsvarlig() {
		return ansvarlig;
	}

	public void setAnsvarlig(String ansvarlig) {
		this.ansvarlig = ansvarlig;
	}

	public LocalDate getPremiereDato() {
		return premiereDato;
	}

	public void setPremiereDato(LocalDate premiereDato) {
		this.premiereDato = premiereDato;
	}

	public int getPris() {
		return pris;
	}

	public void setPris(int pris) {
		this.pris = pris;
	}

	public ArrayList<Event> getEvents() {
		return events;
	}

	public void addEvent(Event event) {
		this.events.add(event);
	}

	public void removeEvent(Event event) {
		this.events.remove(event);
	}

	public int getAntalLangeEvents() {
		int antalEvents = 0;
		for (Event e : events) {
			if (ChronoUnit.DAYS.between(e.getStartDato(), e.getSlutDato()) > 3) {
				antalEvents++;
			}
		}
		return antalEvents;
	}

	public void createEvent(String navn, LocalDate startDato, LocalDate slutDato) throws WrongDateException {
		if (slutDato.isBefore(startDato)) {
			throw new WrongDateException();
		} else {
			Event e = new Event(navn, startDato, slutDato, this);
			events.add(e);
		}
	}

	public Event eventOnDate(LocalDate dato) {
		boolean found = false;
		int i = 0;
		while (!found && i < events.size()) {
			if (dato.isAfter(events.get(i).getStartDato()) && dato.isBefore(events.get(i).getSlutDato())
					|| dato.equals(events.get(i).getStartDato()) || dato.equals(events.get(i).getSlutDato())) {
				found = true;
			} else {
				i++;
			}
		}
		if (found) {
			return events.get(i);
		} else {
			return null;
		}
	}
}
