package semesterprøve_2014_01;

import java.time.LocalDate;
import java.util.ArrayList;

public class Event {
	private String navn;
	private LocalDate startDato;
	private LocalDate slutDato;
	private Arrangement arrangement;
	private ArrayList<Ressource> ressourcer = new ArrayList<Ressource>();

	public Event(String navn, LocalDate startDato, LocalDate slutDato, Arrangement arrangement) {
		this.navn = navn;
		this.startDato = startDato;
		this.slutDato = slutDato;
		this.arrangement = arrangement;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public LocalDate getStartDato() {
		return startDato;
	}

	public void setStartDato(LocalDate startDato) {
		this.startDato = startDato;
	}

	public LocalDate getSlutDato() {
		return slutDato;
	}

	public void setSlutDato(LocalDate slutDato) {
		this.slutDato = slutDato;
	}

	public Arrangement getArrangement() {
		return arrangement;
	}

	public void setArrangement(Arrangement arrangement) {
		this.arrangement = arrangement;
	}

	public ArrayList<Ressource> getRessourcer() {
		return ressourcer;
	}

	public void addResource(Ressource ressource) {
		this.ressourcer.add(ressource);
	}

	public void removeResource(Ressource ressource) {
		this.ressourcer.remove(ressource);
	}

}
