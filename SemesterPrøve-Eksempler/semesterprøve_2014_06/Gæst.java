package semesterprøve_2014_06;

import java.util.ArrayList;

public class Gæst {
	private String navn;
	private int værelsesNummer;
	private ArrayList<Bestilling> bestillinger = new ArrayList<Bestilling>();

	public Gæst(String navn, int værelsesNummer) {
		this.navn = navn;
		this.værelsesNummer = værelsesNummer;
	}

	public String getNavn() {
		return navn;
	}

	public int getVærelsesNummer() {
		return værelsesNummer;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public void setVærelsesNummer(int værelsesNummer) {
		this.værelsesNummer = værelsesNummer;
	}

	public ArrayList<Bestilling> getBestillinger() {
		return bestillinger;
	}

	public void addBestilling(Bestilling bestilling) {
		bestillinger.add(bestilling);
	}

	public void removeBestilling(Bestilling bestilling) {
		bestillinger.remove(bestilling);
	}

	public double pris() {
		double sum = 0.0;
		for (int i = 0; i < bestillinger.size(); i++) {
			sum = sum + bestillinger.get(i).pris();
		}
		return sum;
	}

	public boolean harBestilt(Vare vare) {
		boolean found = false;
		int i = 0;
		int j = 0;
		while (!found && i < bestillinger.size()) {
			while (j < bestillinger.get(i).getBestillingslinjer().size()) {
				if (bestillinger.get(i).getBestillingslinjer().get(j).getVare().equals(vare)) {
					found = true;
				} else {
					i++;
				}
			}
		}
		return found;
	}

	public ArrayList<String> ikkeBetalteBestillinger() {
		ArrayList<String> ikkeBetalte = new ArrayList<String>();
		for (Bestilling b : bestillinger) {
			if (!b.isBetalt()) {
				for (BestillingsLinje bl : b.getBestillingslinjer()) {
					ikkeBetalte.add(b.getDato() + " " + bl.getAntal() + " " + bl.getVare().getNavn() + " "
							+ bl.getVare().getMærke() + " " + bl.getVare().getPris());
				}
			}
		}
		return ikkeBetalte;
	}

	@Override
	public String toString() {
		return this.navn + " " + this.værelsesNummer;
	}
}