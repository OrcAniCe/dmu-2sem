package semesterprøve_2014_06;

public class BestillingsLinje {
	private int antal;
	private Bestilling bestilling;
	private Vare vare;

	public BestillingsLinje(int antal, Bestilling bestilling, Vare vare) {
		this.antal = antal;
		this.bestilling = bestilling;
		this.vare = vare;
	}

	public int getAntal() {
		return antal;
	}

	public Bestilling getBestilling() {
		return bestilling;
	}

	public Vare getVare() {
		return vare;
	}

	public double pris() {
		return this.vare.getPris() * this.antal;
	}

}
