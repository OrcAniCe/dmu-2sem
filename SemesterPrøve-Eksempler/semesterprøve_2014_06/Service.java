package semesterprøve_2014_06;

import java.time.LocalDate;
import java.util.ArrayList;

public class Service {

	public static Gæst createGæst(String navn, int værelsesNummer) {
		Gæst gæst = new Gæst(navn, værelsesNummer);
		Dao.addGæst(gæst);
		return gæst;
	}

	public static Vare createVare(String navn, String mærke, double mængde, double pris) {
		Vare vare = new Vare(navn, mærke, mængde, pris);
		Dao.addVare(vare);
		return vare;
	}

	public static Bestilling createBestilling(Gæst gæst, LocalDate dato, boolean betalt) {
		Bestilling bestilling = new Bestilling(dato, betalt);
		gæst.addBestilling(bestilling);
		return bestilling;
	}

	public static BestillingsLinje createBestillingslinje(Bestilling bestilling, Vare vare, int antal) {
		BestillingsLinje bestillingslinje = new BestillingsLinje(antal, bestilling, vare);
		bestilling.addBestillingsLinje(bestillingslinje);
		return bestillingslinje;
	}

	public static ArrayList<Gæst> getGæster() {
		return new ArrayList<Gæst>(Dao.getGæster());
	}

	public static void createSomeObjects() {
		Vare cocacola = createVare("Cola", "CocaCola", 0.5, 28);
		Vare pepsi = createVare("Cola", "Pepsi", 0.5, 26);
		Vare tuborg = createVare("Øl", "Tuborg", 0.33, 33);
		Vare carlsberg = createVare("Øl", "Carlsberg", 0.33, 48);

		Gæst ib = createGæst("Ib", 17);
		createGæst("Eva", 12);
		createGæst("Lene", 8);

		Bestilling b1 = createBestilling(ib, LocalDate.of(2014, 6, 15), false);
		Bestilling b2 = createBestilling(ib, LocalDate.of(2014, 6, 16), false);

		createBestillingslinje(b1, tuborg, 3);
		createBestillingslinje(b1, cocacola, 2);
		createBestillingslinje(b1, pepsi, 2);

		createBestillingslinje(b2, tuborg, 2);
		createBestillingslinje(b2, pepsi, 3);
		createBestillingslinje(b2, carlsberg, 4);
	}

}
