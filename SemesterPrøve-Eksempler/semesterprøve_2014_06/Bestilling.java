package semesterprøve_2014_06;

import java.time.LocalDate;
import java.util.ArrayList;

public class Bestilling {
	private LocalDate dato;
	private boolean betalt;
	private ArrayList<BestillingsLinje> bestillingslinjer = new ArrayList<BestillingsLinje>();

	public Bestilling(LocalDate dato, boolean betalt) {
		this.dato = dato;
		this.betalt = betalt;
	}

	public LocalDate getDato() {
		return dato;
	}

	public boolean isBetalt() {
		return betalt;
	}

	public ArrayList<BestillingsLinje> getBestillingslinjer() {
		return bestillingslinjer;
	}

	public void addBestillingsLinje(BestillingsLinje bestillingslinje) {
		bestillingslinjer.add(bestillingslinje);
	}

	public void removeBestillingslinje(BestillingsLinje bestillingslinje) {
		bestillingslinjer.remove(bestillingslinje);
	}

	public double pris() {
		double sum = 0.0;
		for (int i = 0; i < bestillingslinjer.size(); i++) {
			sum = sum + bestillingslinjer.get(i).pris();
		}
		return sum;
	}

}
