package semesterprøve_2014_06;

import java.util.ArrayList;

public class Vare {
	private String navn;
	private String mærke;
	private double mængde;
	private double pris;
	private ArrayList<BestillingsLinje> bestillingslinjer = new ArrayList<BestillingsLinje>();

	public Vare(String navn, String mærke, double mængde, double pris) {
		this.navn = navn;
		this.mærke = mærke;
		this.mængde = mængde;
		this.pris = pris;
	}

	public String getNavn() {
		return navn;
	}

	public String getMærke() {
		return mærke;
	}

	public double getMængde() {
		return mængde;
	}

	public double getPris() {
		return pris;
	}

	public ArrayList<BestillingsLinje> getBestillingslinjer() {
		return bestillingslinjer;
	}

	public void addBestillingsLinje(BestillingsLinje bestillingslinje) {
		bestillingslinjer.add(bestillingslinje);
	}

	public void removeBestillingslinje(BestillingsLinje bestillingslinje) {
		bestillingslinjer.remove(bestillingslinje);
	}

}
