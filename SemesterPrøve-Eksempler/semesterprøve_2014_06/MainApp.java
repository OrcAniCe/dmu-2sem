package semesterprøve_2014_06;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class MainApp extends Application {
	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Hotel bestillinger");
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane, 700, 400);
		stage.setScene(scene);
		stage.show();
	}

	// -------------------------------------------------------------------------

	private ListView<Gæst> lvwGæster;
	private ListView<String> lvwIkkeBetalte;
	private TextField txfGæst;
	private TextField txfVærelse;
	private Button btnOpret;
	private Button btnOpdater;
	private Button btnVis;

	private final Controller controller = new Controller();

	private void initContent(GridPane pane) {
		// show or hide grid lines
		pane.setGridLinesVisible(false);
		// set padding of the pane
		pane.setPadding(new Insets(20));
		// set horizontal gap between components
		pane.setHgap(10);
		// set vertical gap between components
		pane.setVgap(10);

		// HBoxes
		HBox HBox1 = new HBox(20);
		pane.add(HBox1, 2, 2, 3, 1);
		HBox1.setAlignment(Pos.CENTER_LEFT);

		HBox HBox2 = new HBox(20);
		pane.add(HBox2, 1, 3, 3, 1);
		HBox2.setAlignment(Pos.CENTER_LEFT);

		// add labels to the pane
		Label lblGæster = new Label("Gæster:");
		pane.add(lblGæster, 0, 0);
		Label lblGæst = new Label("Gæst:");
		pane.add(lblGæst, 1, 0);
		Label lblVærelse = new Label("Værelse:");
		pane.add(lblVærelse, 1, 1);
		Label lblIkkeBetalte = new Label("Ikke betalte bestillinger:");
		HBox2.getChildren().add(lblIkkeBetalte);
		//		pane.add(lblIkkeBetalte, 1, 4, 2, 1);

		// add a listView to the pane(at col=1, row=0)
		lvwGæster = new ListView<Gæst>();
		pane.add(lvwGæster, 0, 1, 1, 5);
		lvwGæster.getItems().setAll(Service.getGæster());

		lvwIkkeBetalte = new ListView<String>();
		pane.add(lvwIkkeBetalte, 2, 5, 1, 5);

		ChangeListener<Gæst> listener = (ov, oldGæst, newGæst) -> this.controller.selectionChanged();
		lvwGæster.getSelectionModel().selectedItemProperty().addListener(listener);
		lvwGæster.getSelectionModel().clearSelection();

		// add a text field to the pane (at col=6, row=0, spanning 2 columns and1 row)
		txfGæst = new TextField();
		pane.add(txfGæst, 2, 0);
		txfVærelse = new TextField();
		pane.add(txfVærelse, 2, 1);

		// initialize and add buttons to the pane
		btnOpret = new Button("Opret");
		HBox1.getChildren().add(btnOpret);
		//		pane.add(btnOpret, 2, 3);
		btnOpdater = new Button("Opdater");
		HBox1.getChildren().add(btnOpdater);
		//		pane.add(btnOpdater, 3, 3);
		btnVis = new Button("Vis");
		HBox2.getChildren().add(btnVis);
		//		pane.add(btnVis, 3, 4);

		// Button actions
		btnOpret.setOnAction(event -> controller.opretGæstAction());
		btnOpdater.setOnAction(event -> controller.opdaterGæstAction());
		btnVis.setOnAction(event -> controller.visIkkeBetalteAction());
	}

	// -------------------------------------------------------------------------

	private class Controller {

		public Controller() {
			Service.createSomeObjects();
		}

		public void opretGæstAction() {
			try {
				String navn = "";
				int værelsesNummer = 0;

				if (!txfGæst.getText().isEmpty()) {
					navn = txfGæst.getText();
				}
				if (!txfVærelse.getText().isEmpty()) {
					værelsesNummer = Integer.parseInt(txfVærelse.getText());
				}
				if (!navn.isEmpty() && værelsesNummer != 0) {
					Service.createGæst(navn, værelsesNummer);
					lvwGæster.getItems().setAll(Service.getGæster());
					txfGæst.clear();
					txfVærelse.clear();
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}

		public void opdaterGæstAction() {
			try {
				Gæst gæst = lvwGæster.getSelectionModel().getSelectedItem();
				if (gæst != null) {
					if (!txfGæst.getText().isEmpty() && !txfVærelse.getText().isEmpty()) {
						gæst.setNavn(txfGæst.getText());
						gæst.setVærelsesNummer(Integer.parseInt(txfVærelse.getText()));
						lvwGæster.getItems().setAll(Service.getGæster());
						txfGæst.clear();
						txfVærelse.clear();
					}
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}

		public void visIkkeBetalteAction() {
			try {
				Gæst gæst = lvwGæster.getSelectionModel().getSelectedItem();
				if (!gæst.ikkeBetalteBestillinger().isEmpty()) {
					lvwIkkeBetalte.getItems().setAll(gæst.ikkeBetalteBestillinger());
				} else {
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Information");
					alert.setHeaderText("Ikke betalte bestillinger");
					alert.setContentText(gæst.getNavn() + " har ingen ikke betalte bestillinger.");
					alert.show();
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}

		// -------------------------------------------------------------------------
		// Selectionchange actions

		private void selectionChanged() {
			lvwIkkeBetalte.getItems().clear();
			Gæst gæstSelection = lvwGæster.getSelectionModel().getSelectedItem();
			if (gæstSelection != null) {
				txfGæst.setText(gæstSelection.getNavn());
				txfVærelse.setText("" + gæstSelection.getVærelsesNummer());
			} else {
				txfGæst.clear();
				txfVærelse.clear();
			}
		}
	}
}
