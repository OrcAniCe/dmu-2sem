package semesterprøve_2014_06;

import java.util.ArrayList;

// Samme som Storage klasse
public class Dao {
	private static ArrayList<Vare> varer = new ArrayList<Vare>();
	private static ArrayList<Gæst> gæster = new ArrayList<Gæst>();

	public static ArrayList<Vare> getVarer() {
		return varer;
	}

	public static void addVare(Vare vare) {
		varer.add(vare);
	}

	public static ArrayList<Gæst> getGæster() {
		return gæster;
	}

	public static void addGæst(Gæst gæst) {
		gæster.add(gæst);
	}

}