package model;

public class TestApp {
	private static int[] intList = { 5, 10, 5, 6, 4, 9, 8 };

	private static int[] nedboerPrUge = { 20, 10, 12, 12, 13, 14, 15, 10, 8, 7, 13, 15, 10, 9, 6, 8, 12, 22, 14, 16, 16,
			18, 23, 12, 0, 2, 0, 0, 78, 0, 0, 0, 34, 12, 34, 23, 23, 12, 44, 23, 12, 34, 22, 22, 22, 22, 18, 19, 21, 32,
			24, 13 };

	static Prefix prefix = new Prefix();

	public static void main(String[] args) {
		// Prefix calc
		double[] averageList = Prefix.prefixAverage(intList);
		System.out.println();
		for (double d : averageList) {
			System.out.print(String.format("%.3f", d) + " ");

		}
		// Nedbør
		Nedboer n = new Nedboer();
		int weeks = 10; // Angiv antal uger

		int n1 = n.besteTreFerieUger(TestApp.nedboerPrUge);
		int n2 = n.bedsteFerieUgerStart(5, TestApp.nedboerPrUge);
		int n3 = n.ensNedboer(TestApp.nedboerPrUge);
		System.out.println();
		System.out.println();
		System.out.println("Bedste tre uger, startuge: " + n1);
		System.out.println("Bedste " + weeks + " uger, startuge: " + n2);
		System.out.println("Flest gentagende identiske uger starter i uge: " + n3);

		// Flag
	}

}
