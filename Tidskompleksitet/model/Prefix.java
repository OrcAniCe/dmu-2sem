package model;

public class Prefix {
	public static double[] prefixAverage(int[] inputNum) {

		double[] sumArray = new double[inputNum.length];
		double sum = 0;
		for (int i = 0; i < inputNum.length; i++) {
			sum += inputNum[i];
			sumArray[i] = sum / (i + 1);
		}
		return sumArray;
	}
}
