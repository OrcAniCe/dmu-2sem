package model;

public class Nedboer {

	/**
	 * Returnerer ugenummeret for den uge i året, hvor man skal starte ferien,
	 * hvis man ønsker den minimale nedbørsmængde i de tre uger
	 *
	 * @return
	 */

	public int besteTreFerieUger(int[] array) {
		int threeWeekSum = 0;
		int bestPeriod = array[0] + array[1] + array[2];
		int weekNum = 0;
		for (int i = 0; i < (array.length - 2); i++) {
			threeWeekSum = (array[i] + array[i + 1] + array[i + 2]);
			if (bestPeriod > threeWeekSum) {
				bestPeriod = threeWeekSum;
				weekNum = i + 1;
			}
		}
		return weekNum;
	}

	/**
	 * Returnerer ugenummeret for den uge i året, hvor man skal starte ferien,
	 * hvis man ønsker den minimale nedbørsmængde i det "antal" uger, der er
	 * angivet i paramtereren
	 *
	 * @return
	 */

	public int bedsteFerieUgerStart(int antal, int[] array) {
		int threeWeekSum = 0;
		int bestPeriod = nWeekHelper(array, antal, 0);
		int weekNum = 0;
		for (int i = 0; i < (array.length - antal); i++) {
			threeWeekSum = nWeekHelper(array, antal, i);
			if (bestPeriod > threeWeekSum) {
				bestPeriod = threeWeekSum;
				weekNum = i + 1;
			}
		}
		return weekNum;
	}

	/** hjælper :) */
	private int nWeekHelper(int[] array, int n, int k) {
		int sum = 0;
		for (int i = k; i < (k + n); i++) {
			sum += array[i];
		}
		return sum;
	}

	/**
	 * Returnerer ugenummeret på den første uge hvor nedbøren har været præcis
	 * den samme flest uger i træk
	 *
	 * @return
	 */
	public int ensNedboer(int[] array) {
		int weekNum = 0;
		int counter = 0;
		int tempCounter = 0;
		for (int i = 0; i < (array.length - 1); i++) {
			while ((i + counter) < (array.length - 1) && array[i] == array[i + counter]) {
				counter++;
			}
			if (counter > tempCounter) {
				tempCounter = counter;
				weekNum = (i + 1);
			} else {
				counter = 0;
			}
		}
		return weekNum;

	}
}
