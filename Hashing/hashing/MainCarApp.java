package hashing;

import java.util.HashSet;
import java.util.Set;

public class MainCarApp {

	public static void main(String[] args) {
		Tank c1 = new Tank("32 321", "Char B1", "Heavy Tank", "Grey");
		Tank c2 = new Tank("12 419", "120mm Gun Heavy Tank T29", "A3", "Green");
		Tank c3 = new Tank("91 324", "75mm Gun Medium Tank M4 'Sherman'", "Medium Tank", "Green");
		Tank c4 = new Tank("31 454", "Panzerkampfwagen III Ausf M mit 5cm KwK L/39 ", "Medium Tank", "Feldgrau");
		Tank c5 = new Tank("42 800", " 12.8cm Selbstfahrlafette auf VK30.01(H)", "Tank Destroyer", "Feldgrau");
		Tank c6 = new Tank("40 241", "Panzerkampfwagen IV Ausf. H mit 7.5cm KwK L/40 ", "Medium Panzer", "Feldgrau");
		Tank c7 = new Tank("12 122", "T-54", "Medium Tank", "Green");
		Tank c8 = new Tank("14 880", "T-72", "Main Battle Tank", "Green");
		Tank c9 = new Tank("18 230", "T-80", "Main Battle Tank", "Sand");
		Tank c10 = new Tank("18 231", "T-90", "Main Battle Tank", "Sand");

		Set<Tank> tankSet = new HashSet<>();

		tankSet.add(c1);
		tankSet.add(c2);
		tankSet.add(c3);
		tankSet.add(c4);
		tankSet.add(c5);
		tankSet.add(c6);
		tankSet.add(c7);
		tankSet.add(c8);
		tankSet.add(c9);
		tankSet.add(c10); // douplicate objects are not added.

		for (Tank t : tankSet) {
			System.out.println(t + "\n");
		}

	}
}
