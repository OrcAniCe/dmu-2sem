package hashing;

import java.util.Objects;

public class Tank {

	private String regNo;
	private String make;
	private String model;
	private String color;

	public Tank(String regNo, String make, String model, String color) {
		this.regNo = regNo;
		this.make = make;
		this.model = model;
		this.color = color;
	}

	public String getRegNo() {
		return regNo;
	}

	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public String toString() {
		return "regNo: " + regNo + "\nmake: " + make + "\nmodel: " + model + "\ncolor: " + color;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.regNo, this.color);
	}

	@Override
	public boolean equals(Object o) {
		Tank tank = (Tank) o;
		return regNo == tank.regNo && color == tank.color;

	}

}
