package hashing;

/**
 * This program demonstrates the hash set class.
 */
public class HashSetDemo {
	public static void main(String[] args) {
		// testHashSetRehash();
		testHashSetChaining();
		// testHashSetLinearProbing();
	}

	public static void testHashSetRehash() {
		HashSetChainingRehash names = new HashSetChainingRehash(5);

		names.add("Harry");
		names.add("Sue");
		names.add("Nina");
		names.add("Susannah");
		names.add("Larry");
		names.add("Eve");
		names.add("Sarah");
		names.add("Adam");
		names.add("Tony");
		names.add("Katherine");
		names.add("Juliet");
		names.add("Romeo");

		System.out.println(names);

		System.out.println("Size: " + names.size());
		System.out.println("Contains Romeo: " + names.contains("Romeo"));

		names.remove("Romeo");
		names.remove("Nina");
		System.out.println("Removed Romeo.\nContains Romeo: " + names.contains("Romeo"));
		System.out.println("Removed Nina.\nContains Nina: " + names.contains("Nina"));
		System.out.println("Size: " + names.size());
		System.out.println();
		System.out.println(names);
	}

	public static void testHashSetChaining() {
		HashSetChaining names = new HashSetChaining(13);

		names.add("Harry");
		names.add("Sue");
		names.add("Nina");
		names.add("Susannah");
		names.add("Larry");
		names.add("Eve");
		names.add("Sarah");
		names.add("Adam");
		names.add("Tony");
		names.add("Katherine");
		names.add("Juliet");
		names.add("Romeo");

		System.out.println(names);

		System.out.println("Size: " + names.size());
		System.out.println("Contains Romeo: " + names.contains("Romeo"));

		names.remove("Romeo");
		names.remove("Nina");
		System.out.println("Removed Romeo.\nContains Romeo: " + names.contains("Romeo"));
		System.out.println("Removed Nina.\nContains Nina: " + names.contains("Nina"));
		System.out.println("Size: " + names.size());
		System.out.println();
		System.out.println(names);
	}

	public static void testHashSetLinearProbing() {
		HashSetLinearProbing names = new HashSetLinearProbing(13);

		names.add("Harry");
		names.add("Sue");
		names.add("Nina");
		names.add("Susannah");
		names.add("Larry");
		names.add("Eve");
		names.add("Sarah");
		names.add("Adam");
		names.add("Tony");
		names.add("Katherine");
		names.add("Juliet");
		names.add("Romeo");

		System.out.println(names);

		System.out.println("Size: " + names.size());
		System.out.println("Contains Romeo: " + names.contains("Romeo"));

		names.remove("Romeo");
		names.remove("Nina");

		System.out.println("Removed Romeo.\nContains Romeo: " + names.contains("Romeo"));

		System.out.println("Removed Nina.\nContains Nina: " + names.contains("Nina"));
		System.out.println(names.size());
		System.out.println();
		names.add("Romeo");
		names.add("Romeo2");
		names.add("Romeo23");
		names.add("Rome43");
		System.out.println(names);
	}
}
