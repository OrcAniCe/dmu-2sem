package hashing;

import java.util.NoSuchElementException;

/**
 * This class implements a hash set using an array and linear probing to handle
 * collisions.
 */
public class HashSetLinearProbing {
	private Object[] buckets;
	private int currentSize;

	private enum State {
		DELETED
	}

	/**
	 * Constructs a hash table.
	 *
	 * @param bucketsLength
	 *            the length of the buckets array
	 */
	public HashSetLinearProbing(int bucketsLength) {
		buckets = new Object[bucketsLength];
		currentSize = 0;
	}

	/**
	 * Tests for set membership.
	 *
	 * @param x
	 *            an object
	 * @return true if x is an element of this set
	 */
	public boolean contains(Object x) {
		boolean found = false;
		for (Object o : buckets) {
			if (x.equals(o)) {
				found = true;
			}
		}
		return found;
	}

	/**
	 * Adds an element to this set.
	 *
	 * @param x
	 *            an object
	 * @return true if x is a new object, false if x was already in the set
	 */
	public boolean add(Object x) {
		int h = hashValue(x);
		if (!contains(x)) {
			for (int i = h; i < buckets.length; i++) {
				if (buckets[i] == State.DELETED || buckets[i] == null) {
					buckets[i] = x;
					currentSize++;
					return true;
				}

			}
			for (int j = 0; j < buckets.length; j++) {
				if (buckets[j] == State.DELETED || buckets[j] == null) {
					buckets[j] = x;
					currentSize++;
					return true;
				}

			}
			System.out.println("no room in list for element: " + x.toString());
			return false;
		} else {
			return false;
		}

	}

	/**
	 * Removes an object from this set.
	 *
	 * @param x
	 *            an object
	 * @return true if x was removed from this set, false if x was not an
	 *         element of this set
	 */
	public boolean remove(Object x) {
		int h = hashValue(x);
		if (!contains(x)) {
			System.out.println("element not in list");
			throw new NoSuchElementException();

		} else {
			for (int i = h; i < buckets.length; i++) {
				if (x.equals(buckets[i])) {
					buckets[i] = State.DELETED;
					currentSize--;
					return true;
				}
			}
			for (int j = 0; j < buckets.length; j++) {
				if (x.equals(buckets[j])) {
					buckets[j] = State.DELETED;
					currentSize--;
					return true;
				}
			}
			return false;
		}
	}

	/**
	 * Gets the number of elements in this set.
	 *
	 * @return the number of elements
	 */
	public int size() {
		return currentSize;
	}

	private int hashValue(Object x) {
		int h = x.hashCode();
		if (h < 0) {
			h = -h;
		}
		h = h % buckets.length;
		return h;
	}

	@Override
	public String toString() {
		String result = "";
		for (int i = 0; i < buckets.length; i++) {
			int value = buckets[i] != null && !buckets[i].equals(State.DELETED) ? hashValue(buckets[i]) : -1;
			result += i + "\t" + buckets[i] + "(h:" + value + ")\n";
		}
		return result;
	}

}
