package opgaver;

/**
 * An implementation of a queue using an array
 */
public class ArrayQueue<T> implements Queue<T> {

	private T[] arr;

	private int total, first, next;

	public ArrayQueue() {
		arr = (T[]) new Object[2];
	}

	private void resize(int capacity) {
		T[] tmp = (T[]) new Object[capacity];

		for (int i = 0; i < total; i++) {
			tmp[i] = arr[(first + i) % arr.length];
		}

		arr = tmp;
		first = 0;
		next = total;
	}

	@Override
	public void enqueue(T ele) {
		if (arr.length == total) {
			resize(arr.length * 2);
		}
		arr[next++] = ele;
		if (next == arr.length) {
			next = 0;
		}
		total++;
	}

	@Override
	public T dequeue() {
		if (total == 0) {
			throw new java.util.NoSuchElementException(" queue is empty");
		}
		T ele = arr[first];
		arr[first] = null;
		if (++first == arr.length) {
			first = 0;
		}
		if (--total > 0 && total == arr.length / 4) {
			resize(arr.length / 2);
		}
		return ele;
	}

	@Override
	public String toString() {
		return java.util.Arrays.toString(arr);
	}

	@Override
	public boolean isEmpty() {
		if (first == -1) {
			return true;
		}
		return false;
	}

	@Override
	public T getFront() {
		return arr[first];
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}

}