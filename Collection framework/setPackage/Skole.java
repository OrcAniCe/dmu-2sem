package setPackage;

import java.util.HashSet;
import java.util.Set;

public class Skole {

	private String navn;
	Set<Studerende> studSet = new HashSet<>();

	public Skole(String navn) {
		this.navn = navn;
	}

	public String getNavn() {
		return navn;
	}

	public void addStud(Studerende stud) {
		studSet.add(stud);
	}

	public void removeStud(Studerende stud) {
		studSet.remove(stud);
	}

	public double gennemsnit() {
		double sum = 0;
		int c = 0;
		for (Studerende s : studSet) {
			for (int k : s.getKarakter()) {
				c++;
				sum += k;
			}
		}
		double result = sum / c;
		return result;
	}

	public Studerende findStud(int studNr) {

		for (Studerende s : studSet) {
			if (s.getStudNr() == studNr) {
				return s;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return navn + studSet;
	}

}
