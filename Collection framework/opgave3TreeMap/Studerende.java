package opgave3TreeMap;

import java.util.ArrayList;
import java.util.List;

public class Studerende implements Comparable<Studerende> {

	private int studNr;
	private String navn;
	private Skole s;
	private List<Integer> karakter = new ArrayList<>();

	public Studerende(int studNr, String navn) {
		this.studNr = studNr;
		this.navn = navn;
	}

	public void setSkole(Skole skole) {
		s = skole;
	}

	public int getStudNr() {
		return studNr;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public List<Integer> getKarakter() {
		return new ArrayList<>(karakter);
	}

	public void addKarakter(int karakter) {
		this.karakter.add(karakter);
	}

	@Override
	public String toString() {
		return navn;
	}

	@Override
	public int compareTo(Studerende o) {
		if (o.studNr < studNr) {
			return -1;
		} else if (o.studNr > studNr) {
			return 1;
		} else {
			return 0;
		}
	}

}
