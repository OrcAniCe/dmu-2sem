package opgave3TreeMap;

import java.util.Map;
import java.util.TreeMap;

public class Skole {

	private String navn;
	Map<Integer, Studerende> studSet = new TreeMap<>();

	public Skole(String navn) {
		this.navn = navn;
	}

	public String getNavn() {
		return navn;
	}

	public void addStud(int studNr, Studerende stud) {
		studSet.put(studNr, stud);
	}

	public void removeStud(Studerende stud) {
		studSet.remove(stud);
	}

	public double gennemsnit() {
		double sum = 0;
		int c = 0;
		for (Integer i : studSet.keySet()) {
			for (Integer k : studSet.get(i).getKarakter()) {
				c++;
				sum += k;
			}
		}
		double result = sum / c;
		return result;
	}

	public Studerende findStud(int studNr) {

		for (Integer i : studSet.keySet()) {
			if (studSet.get(i).getStudNr() == studNr) {
				return studSet.get(i);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return navn + studSet;
	}

}
