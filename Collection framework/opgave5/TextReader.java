package opgave5;

import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class TextReader {

	public static void main(String[] args) throws IOException {

		Set<String> textSet = new TreeSet<>();
		Map<String, Integer> textMap = new TreeMap<>();

		FileReader fr = new FileReader("src/opgave5/fileText.txt");
		Scanner scan = new Scanner(fr);

		scan.useDelimiter("[^a-zA-ZæøåÆØÅ]+");

		while (scan.hasNext()) {
			String s = scan.next();
			textSet.add(s);
		}

		System.out.println(textSet.toString());
		System.out.println(textSet.size());
		scan.close();
		fr.close();

		// opg 6 VVVVVVVVVVVVVVVVVVVVVVVVVVVV

		FileReader fr1 = new FileReader("src/opgave5/fileText2.txt");
		Scanner scan1 = new Scanner(fr1);

		scan1.useDelimiter("[^a-zA-ZæøåÆØÅ]+");

		while (scan1.hasNext()) {
			String s = scan1.next();
			if (!textMap.containsKey(s)) {
				textMap.put(s, 1);
			} else {
				int k = textMap.get(s).intValue();
				k++;
				textMap.put(s, k);
			}

		}

		System.out.println(textMap.toString());

	}
}
