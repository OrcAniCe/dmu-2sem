package opgave4;

import java.util.Comparator;

public class StudComp implements Comparator<Studerende> {

	@Override
	public int compare(Studerende o1, Studerende o2) {
		return o1.getNavn().compareTo(o2.getNavn());
	}

}
