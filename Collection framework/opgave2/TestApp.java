package opgave2;

public class TestApp {

	public static void main(String[] args) {
		Studerende st0 = new Studerende(23431, "a");
		Studerende st1 = new Studerende(53453, "b");
		Studerende st2 = new Studerende(65442, "c");
		Studerende st3 = new Studerende(98232, "d");

		st0.addKarakter(4);
		st0.addKarakter(7);
		st0.addKarakter(7);
		st0.addKarakter(10);

		st1.addKarakter(2);
		st1.addKarakter(2);
		st1.addKarakter(4);
		st1.addKarakter(4);
		st1.addKarakter(2);

		st2.addKarakter(10);
		st2.addKarakter(12);
		st2.addKarakter(10);

		st3.addKarakter(0);
		st3.addKarakter(7);
		st3.addKarakter(7);
		st3.addKarakter(2);
		st3.addKarakter(-3);

		Skole s0 = new Skole("Test Skole");

		s0.addStud(23431, st0);
		s0.addStud(53453, st1);
		s0.addStud(65442, st2);
		s0.addStud(98232, st3);

		System.out.println(String.format("%.2f", s0.gennemsnit()));
		System.out.println(s0);
		System.out.println(s0.findStud(98232));
	}

}