package opgave1;
import java.util.HashSet;
import java.util.Set;

public class HeshSetTester {

	public static void main(String[] args) {
		Set<Integer> hashSet = new HashSet<>();

		hashSet.add(34);
		hashSet.add(12);
		hashSet.add(23);
		hashSet.add(45);
		hashSet.add(67);
		hashSet.add(34);
		hashSet.add(98);

		for (int i : hashSet) {
			System.out.println(i);
		}
		System.out.println(" ");

		hashSet.add(23);

		for (int i : hashSet) {
			System.out.println(i);
		}
		System.out.println(" ");

		hashSet.remove(67);

		for (int i : hashSet) {
			System.out.println(i);
		}
		System.out.println(" ");

		System.out.println(hashSet.contains(23));

		System.out.println(hashSet.size());

	}

}
